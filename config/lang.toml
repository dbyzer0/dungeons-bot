# All top level items need to be added in src/lang.py

[adventure]
    [adventure.result]
    default = [
        "You left with a sense of adventure.",
        "You embarked on a journey of adventure.",
        "You embarked on an unexpected journey.",
    ]

[attack_character]
    # 0: weapon name
    # 1: enemy name
    # 2: damage done
    [attack_character.result]
    default = [
        "You used your {0} to hit the {1} for {2} damage.",
        "You managed to hit the {1} for {2} damage with your {0}.",
        "You successfully hit the {1} with your {0} for {2} damage.",
    ]
    crit_success = [
        "You used your {0} with incredible skill, critically hitting the {1} for {2} damage.",
        "You handled your {0} with expert precision, critically hitting the {1} for {2} damage.",
        "You handled your {0} with grace, dealing a critical blow to the {1} for {2} damage.",
        "You expertly wielded your {0} in the face of the {1}, dealing {2} critical damage.",
        "You utilized your {0} perfectly, dealing {2} critical damage to the {1}.",
    ]
    crit_fail = [
        "You fumbled with your {0}, missing the {1} entirely.",
        "You couldn't even get your {0} out in time to hit the {1} at all.",
    ]
    surprise_hit = [
        "You got the jump on the {1} with your {0}, dealing {2} damage.",
    ]
    surprise_miss = [
        "You got the jump on the {1} with your {0}, but you missed.",
    ]
    miss = [
        "You used your {0} to attack the {1} but missed.",
    ]
    # 0: attack type emoji
    # 1: damage dice
    # 2: disadvantage/advantage
    [attack_character.type]
    melee_one = [
        "{0} Melee, one-handed [{1}] {2}",
    ]
    melee_two = [
        "{0} Melee, two-handed [{1}] {2}",
    ]
    melee_unarmed = [
        "{0} Melee, unarmed [{1}] {2}",
    ]
    melee_thrown = [
        "{0} Ranged, thrown [{1}] {2}",
    ]
    melee_improvised = [
        "{0} Ranged, improvised [{1}] {2}",
    ]
    ranged_one = [
        "{0} Ranged, one-handed [{1}] {2}",
    ]
    ranged_two = [
        "{0} Ranged, two-handed [{1}] {2}",
    ]
    ranged_improvised = [
        "{0} Melee, improvised [{1}] {2}",
    ]
    mixed = [
        "{0} Mixed [{1}] {2}",
    ]
    # 0: spell slot emoji
    # 1: spell name
    # 2: damage dice
    # 3: disadvantage/advantage
    spell = [
        "{0} {1} [{2}] {3}",
    ]

[attack_monster]
    # 0: monster name
    # 1: attack name
    # 2: damage done
    [attack_monster.result]
    default = [
        "The {0} used {1}, dealing {2} damage to you.",
        "You took {2} damage from the {0}'s {1}.",
        "The {0} attacked with its {1}, you took {2} damage.",
        "You were hit by the {0}'s {1} attack, you took {2} damage.",
    ]
    crit_success = [
        "The {0} attacked with great ferocity with its {1}, dealing {2} critical damage to you.",
        "The {0} used its {1} perfectly, dealing {2} critical damage to you.",
        "The {0} dealt a critical blow to you with its {1}, you took {2} damage.",
    ]
    crit_fail = [
        "The {0} tried to attack with its {1}, but fumbled over itself.",
        "The {0} attacked with {1}, but it missed entirely.",
        "The {0} made a poor attempt to attack with its {1}, you came through unscathed.",
    ]
    surprise_hit = [
        "The {0} got the jump on you and attacked you with its {1}, dealing {2} damage.",
    ]
    surprise_miss = [
        "The {0} got the jump on you and attacked you with its {1}, but it missed.",
    ]
    miss = [
        "The {0} attacked with {1}, but missed.",
        "The {0} attacked with {1}, but you were able to avoid any damage.",
    ]

[avoid]
    # 0: monster name
    [avoid.result]
    success = [
        "You snuck past the {0}.",
        "You managed to avoid the {0}.",
        "You managed to slip past the {0} unnoticed.",
        "You deftly avoided the {0}'s sight.",
    ]
    failure = [
        "You tried to sneak past the {0}, but it saw you.",
        "The {0} saw you as you tried to sneak past.",
        "You were caught by the {0} before you could escape.",
        "You tried to avoid the {0}, but it detected your presence."
    ]

# 0: character long bio
# 1: campaign id
# 2: campaign duration
# 3: campaign vote count
[bio_campaign]
default = [
    "{0}\n\n{1} {2} {3}",
]

[bio_character]
# 0: character emoji and title
# 1: hitpoints
# 2: level (xp)
# 3: hit dice
# 4: weapon
# 5: armor
# 6: spell slots
# 7: gold
# 8: abilities
long = [
    "{0}\n{1}\n{2} {3}\n{4}\n{5}\n{6}\n{7}\n{8}",
]
# 0: grave emoji
# 1: character name
# 2: hitpoints
# 3: experience level
# 4: gold
# 5: equipment
# 6: abilities
# 7: kills
short = [
    "{0} Here lies {1}\n\n{2} {3} {4}\n{5}\n{6}\n{7}"
]
# 0: hitpoints
# 1: weapon dice
# 2: AC
# 3: level
bar = [
    "{0} {1} {2} {3}"
]

[bio_monster]
# 0: monster emoji and name
# 1: hitpoint bar
# 2: armor
# 3: status effects
# 4: challenge rating
# 5: xp
long = [
    "{0}\n{1}\n{2} {3}\n{4}\n{5}",
]
# 0: monster emoji and name
# 1: hitpoints
# 2: flying/swimming
# 3: challenge rating
# 4: xp
short = [
    "{0}\n{1} {2} {3} {4}",
]
# 0: hitpoints
# 1: challenge rating
# 2: armor class
bar = [
    "{0} {1} {2}"
]

[chest]
    [chest.prompt]
    default = [
        "You come across an interesting looking chest...",
        "You see what looks to be a treasure chest peeking out of a hole in the roadside...",
        "You see a chest at the bottom of a nearby stream..."
    ]
    [chest.mimic.prompt]
    default = [
        "You see what looks to be a treasure chest, but you could have sworn you saw it move...",
        "You see a chest buried in the sand. The sand shifts as you approach...",
    ]
    [chest.mimic.result]
    attack = [
        "You attacked the mimic.",
    ]
    investigate = [
        "You tried to open the chest, but it turned out to be a mimic.",
    ]
    [chest.treasure.prompt]
    default = [
        "You come across a treasure chest hidden in the brush...",
    ]
    [chest.treasure.result]
    attack = [
        "You destroyed the treasure.",
    ]
    investigate = [
        "You opened the chest, and discovered a trove of treasure.",
    ]

[command_reply]
blacklisted = [
    "Your name will no longer be chosen for new characters. Use '!whitelist' to remove your account from the blacklist.\nPlease note this is only for your account, other similar display names could be chosen from other users."
]
whitelisted = [
    "Your name is no longer blacklisted. If desired, use '!blacklist' to blacklist your account again."
]

[create]
    [create.prompt]
    default = [
        "This is the legend of...",
        "This is the tale of...",
        "This is the chronicle of...",
        "This is the story of...",
    ]
    # 0: character name
    [create.result]
    default = [
        "{0} was born!",
        "{0} has arrived in town!",
        "Our hero, {0}, has arrived!",
    ]

[death_character]
    [death_character.result]
    default = [
        "You got knocked unconscious and ultimately succumbed to your injuries.",
        "You got knocked down and your injuries were too great.",
        "You fell unconscious and couldn't find the strength to get back up.",
        "You fell unconscious and drifted away.",
    ]
    instant = [
        "You suffered a heavy blow and were instantly killed.",
        "You took heavy damage, you died instantly.",
        "You took damage too great and died instantly.",
        "You suffered heavy damage and were killed.",
    ]
    unconscious = [
        "You got knocked unconscious, but managed to get back up.",
        "You went down for a moment, but managed to get back up.",
        "You got knocked down, but somehow found the strength to get back up.",
        "You briefly lost consciousness, but managed to regain your footing.",
    ]

[death_monster]
    # 0: monster name
    [death_monster.result]
    default = [
        "The {0} died.",
        "The {0} fell, ending the battle.",
        "You killed the {0}.",
        "You won the battle, killing the {0}.",
    ]
    # 0: loot name
    # 1: monster name
    # 2: effect string
    loot_effect = [
        "You found a {0} in the remains of the {1}, {2}.",
    ]

[effects_character]
# 0: amount of HP healed
healing = [
    "healing you for {0} HP",
]
# 0: new strength value
# 1: duration
strength_increased = [
    "increasing your strength to {0} for {1}",
]
# 0: duration
flying = [
    "allowing you to fly through the air for {0}",
]
# 0: amount of HP
# 1: attack/saving bonus
# 2: duration
heroism = [
    "gaining {0} temporary hit points and +{1} bonus to attack rolls and saving throws for {2}",
]
# 0: duration
invisibility = [
    "becoming invisible for {0}",
]
# 0: damage type
# 1: duration
resistance = [
    "becoming resistant to {0} damage for {1}",
]
# 0: duration
haste = [
    "gaining doubled move speed, +2 AC bonus, advantage on Dexterity saving throws, and an additional action for {0}",
]

[fight]
    # 0: monster emoji and name
    [fight.prompt]
    default = [
        "In the distance you see a {0}",
        "Ahead of your path, you see a {0}",
        "You encounter a {0}",
    ]
    # 0: monster name
    [fight.result]
    default = [
        "You engaged the {0}!",
        "You confronted the {0}!",
    ]

# 0: emoji
[options]
adventure = "{0} Adventure!"
attack = "{0} Attack!"
attack_option = "{0}" # these are specified above in [attack_character] and [attack_monster]
avoid = "{0} Avoid"
# 1: item
buy_item = "{0}"
# 1: character title
character = "{0} {1}"
fight = "{0} Fight!"
investigate = "{0} Venture closer"
leave = "{0} Leave"
# 1: heart, 2: dice
long_rest = "{0} Long Rest (+{1} +{2})"
retire = "{0} Retire (campaign ends)"
run = "{0} Run!"
# 0: emoji
# 1: resulting AC
# 2: net gold
# 3: gold emoji
sell_armor = "{0} Sell Armor [{1}] ({2} {3})"
# 0: silver emoji
# 1: cost
silver = "{0} Silver Weapon ({1})"
shop = "{0} Shop"
# 0: emoji
# 1: heart, 2: dice
short_rest = "{0} Short Rest (+{1} -{2})"
# 0: emoji
# 1: item
take_item = "{0} {1}"
talk = "{0} Talk"
town = "{0} Go to Town"

[purchase]
    # 0: purchase equipment name
    [purchase.result]
    default = [
        "You decided to buy the {0} and left the shop.",
        "You found the {0} a great deal and decided to buy it.",
        "You left the shop with the {0} in-hand.",
        "You purchased the {0} and left the shop.",
    ]

[rest_long]
    # 0: hitpoints regained
    # 1: hitdice regained
    [rest_long.result]
    default = [
        "You slept long through the night, regaining {0} hitpoints and {1} hitdice.",
        "After a long deserved rest, you woke up feeling refreshed and with {0} more hitpoints and {1} hitdice.",
        "You rested peacefully through the night, regaining {0} hitpoints and {1} hitdice.",
        "You slept soundly through the night, regaining {0} hitpoints and {1} hitdice.",
        "You drifted off to sleep, regaining {0} hitpoints and {1} hitdice when you woke.",
    ]
    # 0: number of effects
    effects = [
        "{0} effects wore off.",
        "{0} effects expired.",
    ]

[rest_short]
    # 0: hitpoints regained
    [rest_short.result]
    default = [
        "You rested for a moment, regaining {0} hitpoints.",
        "You had a moment to take a breather, you regained {0} hitpoints.",
        "You took a short rest, restoring {0} hitpoints.",
        "You took a few moments to relax, restoring {0} hitpoints.",
        "You sat down and caught your breath, regaining {0} hitpoints.",
    ]
    # 0: number of effects
    effects = [
        "{0} effects wore off.",
        "{0} effects expired.",
    ]
    no_hit_dice = [
        "You had no hit dice to use.",
    ]

[retire]
default = [
    "You retired to the countryside.",
    "After a successful career, you retired.",
    "You gave up your life on adventuring and monster killing.",
    "You died in peace in your house in the countryside.",
]

# 0: reward(s)
[reward]
default = [
    "You gained {0}.",
    "You found {0}.",
    "You were rewarded {0}.",
    "You received {0}.",
]

[run_character]
    [run_character.result]
    # 0: monster name
    # 1: attack name
    # 2: damage amount
    crit_failure = [
        "You were too slow to run away.",
        "You could not get away fast enough.",
        "You did not have the speed to get away.",
    ]
    failure = [
        "You tried to run, but got hit for {2} from the {0}'s {1} attack and were forced to fight.",
        "You tried to flee, but were struck by the {0}'s {1} for {2} damage. You had no choice but to fight.",
        "Your attempt to run was halted by the {0}'s {1} attack, you took {2} damage.",
        "Your effort to escape was foiled by the {0}'s {1}, you took {2} damage.",
    ]
    success = [
        "You ran away, but the {0} hit you for {2} with its {1}.",
        "The {0} used its {1} while you ran away, you took {2} damage.",
        "The {0} managed to hit you with its {1} for {2} damage while you ran.",
    ]
    crit_success = [
        "You successfully ran away!",
        "You ran away!",
        "You easily managed to run away!",
        "You deftly got away!",
    ]

[sell]
    [sell.result]
    # 0: amount of GP gained
    # 1: gold emoji
    armor = [
        "You sold your armor, gaining {0} {1}."
    ]

[shop]
    [shop.prompt]
    default = [
        "You step into a local shop, what catches your eye?",
        "You browse the wares of a shop, what do you want to purchase?",
        "You enter a shop and see a variety of items, what do you want to buy?",
        "The shopkeeper greets you as you enter, what catches your eye?",
        "The shopkeeper nods in greeting as you enter, what would you like?",
    ]
    [shop.result]
    default = [
        "You walked to a local shop.",
        "You asked for directions to a nearby shop.",
        "You found a shop.",
    ]
    empty = [
        "The shop is empty.",
        "The shop had no items that would interest you.",
    ]
    leave = [
        "You left the shop.",
    ]

[stranger]
    [stranger.prompt]
    default = [
        "A hooded stranger approaches you with an offer of wares...",
        "A stranger approaches you asking to talk...",
        "A stranger whispers to you as you pass them...",
    ]
    [stranger.bandit.prompt]
    default = [
        "A stranger approaches you promising riches, but they look a little suspicious...",
        "A suspicious stranger crosses your path and asks to talk...",
    ]
    [stranger.bandit.result]
    attack = [
        "The stranger turned out to be a bandit and you attacked them.",
        "A bandit was disguised as a harmless stranger but you attacked first.",
    ]
    talk = [
        "The stranger turned out to be a bandit and they attacked you.",
    ]
    [stranger.trader.prompt]
    default = [
        "A strange trader approaches you for a sale...",
        "As you pass a stranger on the road, they ask you to look at their wares...",
    ]
    [stranger.trader.result]
    attack = [
        "The stranger turned out to be a harmless trader, so they fled when you tried to attack.",
        "The mysterious figure was actually an innocent vendor. Your aggression caused them to flee.",
        "The unfamiliar wanderer was a harmless merchant and they quickly fled from your attack.",
        "As you launched an attack at the stranger they turned and fled.",
    ]
    talk = [
        "The stranger turned out to be a trader and they offered you a deal.",
        "The stranger turned out to be a trader and they offered you a trade.",
        "Your instincts turned out to be correct, as the stranger turned out to be a harmless merchant.",
    ]

[town]
    [town.prompt]
    default = [
        "You are safe in a small village, what would you like to do?",
        "You are safe in a town, what would you like to do?",
        "You find refuge in a bustling town, what would you like to do?",
        "The walls of a town provide a sense of security, what would you like to do?",
    ]
    [town.result]
    default = [
        "You traveled to a nearby town.",
        "You made the journey to a nearby town.",
        "You found your way to a nearby village.",
    ]

[trader]
    [trader.prompt]
    default = [
        "The trader presents some options for purchase...",
        "The trader has a few items they're willing to part with.",
    ]
    [trader.result]
    # 0: item
    default = [
        "You bought the {0} from the trader.",
        "You bartered for the {0} from the trader.",
    ]
    # 0: item
    # 1: effect result string
    effect = [
        "You bought and used the {0} from the trader, {1}.",
    ]
    silver = [
        "The trader silvered your {0}.",
    ]
    leave = [
        "The trader didn't have anything you needed, so you left.",
        "You left the trader.",
    ]
    empty = [
        "The trader had no items for you, so you left.",
    ]

[treasure]
    [treasure.prompt]
    # 0: amount of items to take
    default = [
        "You scour the treasure chest and discover a few items. You can carry up to {0}.",
    ]
    [treasure.result]
    # 0: item
    default = [
        "You took the {0}.",
    ]
    # 0: item
    # 1: effect result string
    effect = [
        "You used the {0}, {1}."
    ]
    leave = [
        "You left the treasure.",
    ]
