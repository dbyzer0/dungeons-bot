Click [here](https://toml.io/en/) for the TOML specification.

# [blacklist.toml](./blacklist.toml)

# [config.toml](./config.toml)

# [emojis.toml](./emojis.toml)

# [lang.toml](./lang.toml)

Configuration file containing the English phrases used for the bot.

The file follows a loose structure:

- **prompts** are *present-tense* statements describing what the character is currently encountering.
- **results** are *past-tense* statements describing what happened to the character as a consequence of the last *option* chosen.
- **options** are typically verbs or locations, typically appearing as poll options.

Keys are either set to another dict, a string, or an array of strings.

Language strings may also contain `{n}`, where `n` corresponds to the arguments passed by the caller when choosing a lang item. Usually `{n}` sections are replaced with emojis or dynamic text generated during gameplay. If a string contains `{n}`, it typically also has a comment specifying which numbers are replaced by what data.

Any new top-level key needs to be added to [src/lang.py](../src/lang.py).
