"""empty message

Revision ID: 012d36030f96
Revises: 8ee75e8f86fd
Create Date: 2024-01-27 22:19:35.565733

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '012d36030f96'
down_revision: Union[str, None] = '8ee75e8f86fd'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('character', sa.Column('languages', sa.JSON(), nullable=True))
    op.add_column('monster', sa.Column('languages', sa.JSON(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('monster', 'languages')
    op.drop_column('character', 'languages')
    # ### end Alembic commands ###
