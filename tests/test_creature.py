import pytest
from attacks import AttackRange, AttackResult, AttackSpell, AttackWeapon
from dice import Dice
from dnd.language import Language
from dnd.spell import Spell
from effect import Effect, EffectEnd, EffectMod, EffectStat, EffectType
from dnd.equipment import UNARMED_EQUIPMENT, Equipment

import dnd.dndclient as dndclient
from creature import Creature
from dnd.feature import Feature
from dnd.proficiency import Proficiency
from dnd.trait import Trait

# Amount of times to perform random tasks
N = 10


class CreatureChild(Creature):

    def armor_class(self, armor: dict = {}) -> int:
        return super().armor_class(armor)

    def attack(self, other: "Creature") -> AttackResult:
        return super().attack(other)

    def emoji(self) -> str:
        return super().emoji()

    def bio(self) -> str:
        return super().bio()

    def short_bio(self) -> str:
        return super().short_bio()


@pytest.fixture
def creature():

    return CreatureChild("Aboleth", 10)


def test_init(creature: CreatureChild) -> None:

    assert creature.name == "Aboleth"
    assert creature.max_hitpoints == 10
    assert creature.hitpoints == 10

    assert creature.proficiency_bonus == 2

    assert len(creature.abilities) > 0
    for ability in creature.abilities:
        assert creature.abilities[ability] == 0

    assert len(creature.saving_throws) > 0
    for saving_throw in creature.saving_throws:
        assert creature.saving_throws[saving_throw] == 0

    assert len(creature.skills) > 0
    for skill in creature.skills:
        assert creature.skills[skill] == 0

    for v in creature.vulnerabilities:
        assert isinstance(v, str)

    for r in creature.resistances:
        assert isinstance(r, str)

    for i in creature.immunities:
        assert isinstance(i, str)

    assert creature.walk_speed >= 0
    assert creature.fly_speed >= 0
    assert creature.climb_speed >= 0
    assert creature.swim_speed >= 0
    assert creature.burrow_speed >= 0


def test_instances(creature: CreatureChild) -> None:

    new_creature = CreatureChild("Test", 10)

    assert new_creature is not creature
    assert new_creature.abilities is not creature.abilities


def test_set_max_hitpoints(creature: CreatureChild) -> None:

    assert creature.set_max_hitpoints(20) == 20
    assert creature.max_hitpoints == 20

    assert creature.set_max_hitpoints(0) == 1
    assert creature.max_hitpoints == 1


def test_set_hitpoints(creature: CreatureChild) -> None:

    assert creature.set_hitpoints(5) == -5
    assert creature.hitpoints == 5
    assert creature.set_hitpoints(10) == 5
    assert creature.hitpoints == 10

    assert creature.set_hitpoints(11) == 0
    assert creature.hitpoints == 10


def test_set_surprised(creature: CreatureChild) -> None:

    assert creature.set_surprised()
    assert creature.is_surprised
    assert not creature.set_surprised(False)
    assert not creature.is_surprised
    assert creature.set_surprised(True)
    assert creature.is_surprised


def test_get_surprised(creature: CreatureChild) -> None:

    assert not creature.get_surprised()
    creature.set_surprised(True)
    assert creature.get_surprised()
    creature.set_surprised(False)
    assert not creature.get_surprised()


def test_resistances(creature: CreatureChild) -> None:

    assert creature.get_resistances() == set()
    creature.resistances.add("acid")
    assert creature.get_resistances() == set(["acid"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.RESISTANCE, "cold"))
    assert creature.get_resistances() == set(["acid", "cold"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.RESISTANCE, "cold"))
    assert creature.get_resistances() == set(["acid", "cold"])


def test_immunities(creature: CreatureChild) -> None:

    assert creature.get_immunities() == set()
    creature.immunities.add("acid")
    assert creature.get_immunities() == set(["acid"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.IMMUNITY, "cold"))
    assert creature.get_immunities() == set(["acid", "cold"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.IMMUNITY, "cold"))
    assert creature.get_immunities() == set(["acid", "cold"])


def test_vulnerabilities(creature: CreatureChild) -> None:

    assert creature.get_vulnerabilities() == set()
    creature.vulnerabilities.add("acid")
    assert creature.get_vulnerabilities() == set(["acid"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.VULNERABILITY, "cold"))
    assert creature.get_vulnerabilities() == set(["acid", "cold"])
    creature.add_effect(Effect(EffectType.POSITIVE, EffectMod.CONSTANT, EffectStat.VULNERABILITY, "cold"))
    assert creature.get_vulnerabilities() == set(["acid", "cold"])


def test_resistant_to(creature: CreatureChild) -> None:

    assert not creature.resistant_to(damage_type="acid")
    creature.resistances.add("acid")
    assert creature.resistant_to(damage_type="acid")
    creature.resistances.clear()

    attack = AttackWeapon(
        Equipment(dndclient.get_equipment("club")),
        Dice(0),
        "bludgeoning",
        0,
        0,
        True,
        AttackRange.MELEE,
        1
    )
    assert not creature.resistant_to(attack=attack)
    creature.resistances.add("bludgeoning")
    assert creature.resistant_to(attack=attack)
    creature.resistances.clear()

    magic_missile = AttackSpell(
        Spell(dndclient.get_spell("magic-missile")),
        0,
        0,
        0,
        0,
        0
    )
    assert not creature.resistant_to(attack=magic_missile)
    creature.resistances.add("force")
    assert creature.resistant_to(attack=magic_missile)
    creature.resistances.clear()
    creature.resistances.add("damage from spells") # archmage only
    assert creature.resistant_to(attack=magic_missile)
    creature.resistances.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("piercing")
    assert creature.resistant_to(weapon=dagger)
    creature.resistances.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("piercing")
    assert not creature.resistant_to(weapon=dagger, weapon_damage_type="slashing")
    creature.resistances.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.resistant_to(weapon=dagger)
    creature.resistances.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("bludgeoning, piercing and slashing from nonmagical attacks not made with silvered weapons")
    assert creature.resistant_to(weapon=dagger)
    dagger.silvered = True
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("bludgeoning, piercing and slashing from nonmagical weapons that aren't adamantine")
    assert creature.resistant_to(weapon=dagger)
    dagger.adamantine = True
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.clear()

    unarmed = Equipment(dndclient.DNDResponse(UNARMED_EQUIPMENT))
    assert not creature.resistant_to(weapon=dagger)
    creature.resistances.add("bludgeoning, piercing and slashing from nonmagical weapons")
    assert not creature.resistant_to(weapon=unarmed)
    creature.resistances.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.resistant_to(weapon=unarmed)
    creature.resistances.clear()


def test_immune_to(creature: CreatureChild) -> None:

    assert not creature.immune_to(damage_type="acid")
    creature.immunities.add("acid")
    assert creature.immune_to(damage_type="acid")
    creature.immunities.clear()

    attack = AttackWeapon(
        Equipment(dndclient.get_equipment("club")),
        Dice(0),
        "bludgeoning",
        0,
        0,
        True,
        AttackRange.MELEE,
        1
    )
    assert not creature.immune_to(attack=attack)
    creature.immunities.add("bludgeoning")
    assert creature.immune_to(attack=attack)
    creature.immunities.clear()

    magic_missile = AttackSpell(
        Spell(dndclient.get_spell("magic-missile")),
        0,
        0,
        0,
        0,
        0
    )
    assert not creature.immune_to(attack=magic_missile)
    creature.immunities.add("force")
    assert creature.immune_to(attack=magic_missile)
    creature.immunities.clear()
    creature.immunities.add("damage from spells") # archmage only
    assert creature.immune_to(attack=magic_missile)
    creature.immunities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("piercing")
    assert creature.immune_to(weapon=dagger)
    creature.immunities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("piercing")
    assert not creature.immune_to(weapon=dagger, weapon_damage_type="slashing")
    creature.immunities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.immune_to(weapon=dagger)
    creature.immunities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("bludgeoning, piercing and slashing from nonmagical attacks not made with silvered weapons")
    assert creature.immune_to(weapon=dagger)
    dagger.silvered = True
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("bludgeoning, piercing and slashing from nonmagical weapons that aren't adamantine")
    assert creature.immune_to(weapon=dagger)
    dagger.adamantine = True
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.clear()

    unarmed = Equipment(dndclient.DNDResponse(UNARMED_EQUIPMENT))
    assert not creature.immune_to(weapon=dagger)
    creature.immunities.add("bludgeoning, piercing and slashing from nonmagical weapons")
    assert not creature.immune_to(weapon=unarmed)
    creature.immunities.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.immune_to(weapon=unarmed)
    creature.immunities.clear()


def test_vulnerable_to(creature: CreatureChild) -> None:

    assert not creature.vulnerable_to(damage_type="acid")
    creature.vulnerabilities.add("acid")
    assert creature.vulnerable_to(damage_type="acid")
    creature.vulnerabilities.clear()

    attack = AttackWeapon(
        Equipment(dndclient.get_equipment("club")),
        Dice(0),
        "bludgeoning",
        0,
        0,
        True,
        AttackRange.MELEE,
        1
    )
    assert not creature.vulnerable_to(attack=attack)
    creature.vulnerabilities.add("bludgeoning")
    assert creature.vulnerable_to(attack=attack)
    creature.vulnerabilities.clear()

    magic_missile = AttackSpell(
        Spell(dndclient.get_spell("magic-missile")),
        0,
        0,
        0,
        0,
        0
    )
    assert not creature.vulnerable_to(attack=magic_missile)
    creature.vulnerabilities.add("force")
    assert creature.vulnerable_to(attack=magic_missile)
    creature.vulnerabilities.clear()
    creature.vulnerabilities.add("damage from spells") # archmage only
    assert creature.vulnerable_to(attack=magic_missile)
    creature.vulnerabilities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("piercing")
    assert creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("piercing")
    assert not creature.vulnerable_to(weapon=dagger, weapon_damage_type="slashing")
    creature.vulnerabilities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("bludgeoning, piercing and slashing from nonmagical attacks not made with silvered weapons")
    assert creature.vulnerable_to(weapon=dagger)
    dagger.silvered = True
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.clear()

    dagger = Equipment(dndclient.get_equipment("dagger"))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("bludgeoning, piercing and slashing from nonmagical weapons that aren't adamantine")
    assert creature.vulnerable_to(weapon=dagger)
    dagger.adamantine = True
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.clear()

    unarmed = Equipment(dndclient.DNDResponse(UNARMED_EQUIPMENT))
    assert not creature.vulnerable_to(weapon=dagger)
    creature.vulnerabilities.add("bludgeoning, piercing and slashing from nonmagical weapons")
    assert not creature.vulnerable_to(weapon=unarmed)
    creature.vulnerabilities.add("bludgeoning, piercing and slashing from nonmagical attacks")
    assert creature.vulnerable_to(weapon=unarmed)
    creature.vulnerabilities.clear()


def test_heal(creature: CreatureChild) -> None:

    creature.set_hitpoints(5)
    assert creature.heal(5) == 5
    assert creature.hitpoints == 10
    assert creature.heal(5) == 0
    assert creature.hitpoints == 10


def test_damage(creature: CreatureChild) -> None:

    assert not creature.damage(5)
    assert creature.hitpoints == 5
    assert creature.damage(5)
    assert creature.hitpoints == 0


def test_temp_hitpoints(creature: CreatureChild) -> None:

    assert creature.temp_hitpoints() == 0
    creature.add_effect(Effect(
        EffectType.POSITIVE,
        EffectMod.ADD,
        EffectStat.TEMP_HP,
        5,
        3600,
        [EffectEnd.DURATION]
    ))
    assert creature.temp_hitpoints() == 5


def test_damage_temp_hitpoints(creature: CreatureChild) -> None:

    creature.add_effect(Effect(
        EffectType.POSITIVE,
        EffectMod.ADD,
        EffectStat.TEMP_HP,
        5,
        3600,
        [EffectEnd.DURATION]
    ))
    assert creature.damage_temp_hitpoints(4) == 0
    assert creature.damage_temp_hitpoints(1) == 0
    assert creature.damage_temp_hitpoints(1) == 1
    assert len(creature.effects) == 0


def test_add_effect(creature: CreatureChild) -> None:

    assert len(creature.effects) == 0

    strength_effect = Effect(
        EffectType.POSITIVE,
        EffectMod.MAX,
        EffectStat.STRENGTH,
        30,
        3600,
        [EffectEnd.DURATION]
    )
    assert creature.add_effect(strength_effect) == [strength_effect]

    temp_effect = Effect(
        EffectType.POSITIVE,
        EffectMod.ADD,
        EffectStat.TEMP_HP,
        5,
        3600,
        [EffectEnd.DURATION]
    )
    creature.add_effect(temp_effect)
    assert len(creature.effects) == 2
    assert creature.effects[1].value == 5
    assert creature.effects[1].effect_mod == EffectMod.CONSTANT
    creature.add_effect(temp_effect)
    assert len(creature.effects) == 2
    assert creature.effects[1].value == 10
    assert creature.effects[1].effect_mod == EffectMod.CONSTANT


def test_clear_effects_end(creature: CreatureChild) -> None:

    visibility_effect1 = Effect(
        EffectType.POSITIVE,
        EffectMod.MAX,
        EffectStat.VISIBILITY,
        False,
        3600,
        [EffectEnd.ATTACK, EffectEnd.SPELL]
    )
    visibility_effect2 = Effect(
        EffectType.POSITIVE,
        EffectMod.MAX,
        EffectStat.VISIBILITY,
        False,
        3600
    )
    creature.add_effect(visibility_effect1)
    creature.add_effect(visibility_effect2)
    assert creature.clear_effects_end(EffectEnd.ATTACK) == [visibility_effect1]
    assert creature.clear_effects_end(EffectEnd.SPELL) == []
    assert len(creature.effects) == 1


def test_clear_effects_duration(creature: CreatureChild) -> None:

    visibility_effect = Effect(
        EffectType.POSITIVE,
        EffectMod.MAX,
        EffectStat.VISIBILITY,
        False,
        3600,
        [EffectEnd.ATTACK, EffectEnd.SPELL]
    )
    assert creature.clear_effects_duration(3600) == []
    assert creature.add_effect(visibility_effect)
    assert visibility_effect.has_effect_end(EffectEnd.DURATION)
    assert creature.clear_effects_duration(600) == []
    assert creature.clear_effects_duration(3000) == [visibility_effect]
    assert len(creature.effects) == 0


def test_add_proficiency(creature: CreatureChild) -> None:

    assert creature.add_proficiency("skill-acrobatics") == Proficiency(dndclient.get_proficiency("skill-acrobatics"))
    assert len(creature.proficiencies) == 1
    assert creature.skills["acrobatics"] == 1
    assert creature.has_proficiency("skill-acrobatics")

    assert creature.add_proficiency("saving-throw-cha") == Proficiency(dndclient.get_proficiency("saving-throw-cha"))
    assert len(creature.proficiencies) == 2
    assert creature.saving_throws["cha"] == 1
    assert creature.has_proficiency("saving-throw-cha")

    assert creature.add_proficiency("alchemists-supplies") == Proficiency(dndclient.get_proficiency("alchemists-supplies"))
    assert len(creature.proficiencies) == 3
    assert creature.has_proficiency("alchemists-supplies")


def test_has_proficiency(creature: CreatureChild) -> None:

    assert not creature.has_proficiency("all-armor")
    assert creature.add_proficiency("all-armor")
    assert creature.has_proficiency("all-armor")


def test_add_feature(creature: CreatureChild) -> None:

    assert len(creature.features) == 0

    assert creature.add_feature("arcane-recovery") == Feature(dndclient.get_feature("arcane-recovery"))
    assert len(creature.features) == 1

    assert creature.add_feature("arcane-tradition") == Feature(dndclient.get_feature("arcane-tradition"))
    assert len(creature.features) == 2

    assert creature.add_feature("arcane-recovery") == None
    assert len(creature.features) == 2


def test_has_feature(creature: CreatureChild) -> None:

    assert not creature.has_feature("arcane-recovery")
    assert creature.add_feature("arcane-recovery")
    assert creature.has_feature("arcane-recovery")


def test_add_trait(creature: CreatureChild) -> None:

    assert not creature.has_trait("high-elf-cantrip")
    assert creature.add_trait("high-elf-cantrip") == Trait(dndclient.get_trait("high-elf-cantrip"))
    assert creature.has_trait("high-elf-cantrip")
    assert creature.add_trait("high-elf-cantrip") == None
    assert creature.has_trait("high-elf-cantrip")
    assert len(creature.traits) == 1

    # proficiency choices
    assert not any([
        creature.has_proficiency("smiths-tools"),
        creature.has_proficiency("brewers-supplies"),
        creature.has_proficiency("masons-tools")
    ])
    assert creature.add_trait("tool-proficiency") == Trait(dndclient.get_trait("tool-proficiency"))
    assert any([
        creature.has_proficiency("smiths-tools"),
        creature.has_proficiency("brewers-supplies"),
        creature.has_proficiency("masons-tools")
    ])

    # proficiency
    assert not creature.has_proficiency("skill-perception")
    assert creature.add_trait("keen-senses") == Trait(dndclient.get_trait("keen-senses"))
    assert creature.has_proficiency("skill-perception")

    # languages
    assert len(creature.languages) == 0
    assert creature.add_trait("extra-language")
    assert len(creature.languages) == 1


def test_has_trait(creature: CreatureChild) -> None:

    assert not creature.has_trait("extra-language")
    assert creature.add_trait("extra-language") == Trait(dndclient.get_trait("extra-language"))
    assert creature.has_trait("extra-language")
    assert creature.add_trait("extra-language") == None
    assert creature.has_trait("extra-language")
    assert len(creature.traits) == 1


def test_add_language(creature: CreatureChild) -> None:

    assert len(creature.languages) == 0
    assert not creature.has_language("common")
    assert creature.add_language("common") == Language(dndclient.get_language("common"))
    assert creature.add_language("common") is None
    assert len(creature.languages) == 1
    assert creature.has_language("common")


def test_has_language(creature: CreatureChild) -> None:

    assert len(creature.languages) == 0
    assert not creature.has_language("common")
    assert creature.add_language("common")
    assert creature.has_language("common")
    assert not creature.has_language("abyssal")


def test_pass_time(creature: CreatureChild) -> None:

    creature.abilities["str"] = 21
    effect1 = Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10, 3600)
    effect2 = Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 1, None)
    creature.effects.append(effect1)
    creature.effects.append(effect2)

    assert creature.ability_score("str") == 32

    assert creature.pass_time(1800) == []
    assert len(creature.effects) == 2
    assert creature.ability_score("str") == 32

    assert creature.pass_time(1800) == [effect1]
    assert len(creature.effects) == 1
    assert creature.ability_score("str") == 22


def test_attack_modifier(creature: CreatureChild) -> None:

    creature.abilities["str"] = 12
    creature.abilities["dex"] = 14

    modifier = creature.attack_modifier("str")
    assert modifier == creature.ability_modifier("str")

    modifier = creature.attack_modifier("dex")
    assert modifier == creature.ability_modifier("dex")

    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.ATTACK_ROLL, 5))
    assert creature.attack_modifier("dex") == creature.ability_modifier("dex") + 5


def test_damage_modifier(creature: CreatureChild) -> None:

    creature.abilities["str"] = 12
    creature.abilities["dex"] = 14

    modifier = creature.damage_modifier("str")
    assert modifier == creature.ability_modifier("str")

    modifier = creature.damage_modifier("dex")
    assert modifier == creature.ability_modifier("dex")

    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.DAMAGE_ROLL, 5))
    assert creature.damage_modifier("dex") == creature.ability_modifier("dex") + 5


def test_weapon_attack_modifier(creature: CreatureChild) -> None:

    creature.abilities["str"] = 12
    creature.abilities["dex"] = 14

    club = Equipment(dndclient.get_equipment("club"))
    modifier = creature.weapon_attack_modifier(club)
    assert modifier == creature.ability_modifier("str")

    crossbow = Equipment(dndclient.get_equipment("crossbow-light"))
    modifier = creature.weapon_attack_modifier(crossbow)
    assert modifier == creature.ability_modifier("dex")

    dagger = Equipment(dndclient.get_equipment("dagger"))
    modifier = creature.weapon_attack_modifier(dagger)
    assert modifier == creature.ability_modifier("dex")

    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.ATTACK_ROLL, 5))
    assert creature.weapon_attack_modifier(club) == creature.ability_modifier("str") + 5


def test_weapon_damage_modifier(creature: CreatureChild) -> None:

    creature.abilities["str"] = 12
    creature.abilities["dex"] = 14

    club = Equipment(dndclient.get_equipment("club"))
    modifier = creature.weapon_damage_modifier(club)
    assert modifier == creature.ability_modifier("str")

    crossbow = Equipment(dndclient.get_equipment("crossbow-light"))
    modifier = creature.weapon_damage_modifier(crossbow)
    assert modifier == creature.ability_modifier("dex")

    dagger = Equipment(dndclient.get_equipment("dagger"))
    modifier = creature.weapon_damage_modifier(dagger)
    assert modifier == creature.ability_modifier("dex")

    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.DAMAGE_ROLL, 5))
    assert creature.weapon_damage_modifier(club) == creature.ability_modifier("str") + 5


def test_ability_score(creature: CreatureChild) -> None:

    creature.abilities["str"] = 21
    creature.abilities["dex"] = 9
    assert creature.ability_score("str") == 21
    assert creature.ability_score("dex") == 9

    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10))
    assert creature.ability_score("str") == 31
    assert creature.ability_score("dex") == 9


def test_ability_modifier(creature: CreatureChild) -> None:

    creature.abilities["cha"] = 10
    assert creature.ability_modifier("cha") == 0

    creature.abilities["cha"] = 11
    assert creature.ability_modifier("cha") == 0
    creature.abilities["cha"] = 12
    assert creature.ability_modifier("cha") == 1
    creature.abilities["cha"] = 20
    assert creature.ability_modifier("cha") == 5

    creature.abilities["cha"] = 9
    assert creature.ability_modifier("cha") == -1
    creature.abilities["cha"] = 8
    assert creature.ability_modifier("cha") == -1
    creature.abilities["cha"] = 0
    assert creature.ability_modifier("cha") == -5

    creature.abilities["str"] = 10
    creature.effects.append(Effect(EffectType.POSITIVE, EffectMod.ADD, EffectStat.STRENGTH, 10))
    assert creature.ability_modifier("str") == 5


def test_ability_check(creature: CreatureChild) -> None:

    creature.abilities["cha"] = 10

    for _ in range(N):
        check = creature.ability_check("cha")
        assert 1 <= check <= 20


def test_saving_throw(creature: CreatureChild) -> None:

    creature.abilities["cha"] = 10

    for _ in range(N):
        throw = creature.saving_throw("cha")
        assert 1 <= throw <= 20

    creature.add_proficiency("saving-throw-cha")
    for _ in range(N):
        throw = creature.saving_throw("cha")
        assert 1 <= throw <= 22


def test_skill_check(creature: CreatureChild) -> None:

    creature.abilities["dex"] = 10
    for _ in range(N):
        assert 1 <= creature.skill_check("acrobatics") <= 20

    creature.abilities["dex"] = 12
    for _ in range(N):
        assert 1 <= creature.skill_check("acrobatics") <= 21

    creature.add_proficiency("skill-acrobatics")
    for _ in range(N):
        assert 1 <= creature.skill_check("acrobatics") <= 23


def test_ability_passive_check(creature: CreatureChild) -> None:

    creature.abilities["dex"] = 10
    assert creature.ability_passive_check("dex") == 10

    creature.abilities["dex"] = 12
    assert creature.ability_passive_check("dex") == 11


def test_skill_passive_check(creature: CreatureChild) -> None:

    creature.abilities["dex"] = 10
    assert creature.skill_passive_check("acrobatics") == 10

    creature.abilities["dex"] = 12
    assert creature.skill_passive_check("acrobatics") == 11

    creature.add_proficiency("skill-acrobatics")
    assert creature.skill_passive_check("acrobatics") == 13


def test_roll_initiative(creature: CreatureChild) -> None:

    i = creature.roll_initiative()
    assert isinstance(i, int)
    assert i == creature.initiative


def test_get_initiative(creature: CreatureChild) -> None:

    i = creature.roll_initiative()
    assert isinstance(i, int)
    assert i == creature.initiative
    assert creature.get_initiative() == i


def test_run(creature: CreatureChild) -> None:

    run = creature.run(creature)
    assert run is not None


def test_avoid(creature: CreatureChild) -> None:

    avoid = creature.avoid(creature)
    assert avoid is not None


def test_recent_rolls(creature: CreatureChild) -> None:

    assert isinstance(creature.get_recent_rolls(), str)
