import lang


def test_globals() -> None:

    # Make sure data actually exists in every key
    def test_keys(dictionary):
        assert len(dictionary) > 0
        if isinstance(dictionary, dict):
            for key in dictionary:
                test_keys(dictionary[key])
        else:
            assert isinstance(dictionary, list) or isinstance(dictionary, str)

    assert len(lang.lang) > 0
    test_keys(lang.lang)


def test_langitem() -> None:

    for key in lang.lang:
        assert lang.LangItem(lang.lang, key)
