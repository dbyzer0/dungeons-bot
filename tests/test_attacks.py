from attacks import AttackResult, AttackSpell
import dnd.dndclient as dndclient
from dnd.spell import Spell


def test_attack_result() -> None:

    result = AttackResult([], 5, dndclient.ActionOutcome.SUCCESS, "test")
    assert result.damage == 5
    assert result.attack_hit == dndclient.ActionOutcome.SUCCESS
    assert result.name == "test"


def test_attack_spell() -> None:

    spell = Spell(dndclient.get_spell("sacred-flame"))
    attack = AttackSpell(spell, 0, 0, 1, 1, 0)
    assert attack.max_damage() == 8
    attack = AttackSpell(spell, 0, 0, 1, 4, 0)
    assert attack.max_damage() == 8

    attack = AttackSpell(spell, 0, 0, 1, 5, 0)
    assert attack.max_damage() == 16
    attack = AttackSpell(spell, 0, 0, 1, 6, 0)
    assert attack.max_damage() == 16

    spell = Spell(dndclient.get_spell("thunderwave"))
    attack = AttackSpell(spell, 0, 0, 1, 1, 0)
    assert attack.max_damage() == 16
    attack = AttackSpell(spell, 0, 0, 2, 1, 0)
    assert attack.max_damage() == 24
    attack = AttackSpell(spell, 0, 0, 10, 1, 0)
    assert attack.max_damage() == 80

    spells = dndclient.get_spells()
    for s in spells:
        spell = Spell(dndclient.get_spell(s["index"]))
        if spell.damage:
            attack = AttackSpell(spell, 0, 0, 1, 1, 0)
            assert attack.max_damage() != 1
