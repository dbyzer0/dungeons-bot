from dnd.subrace import Subrace
import dnd.dndclient as dndclient


def test_init() -> None:

    prev_subrace = None
    for r in dndclient.get_subraces():
        dnd_subrace = dndclient.get_subrace(r["index"])
        subrace = Subrace(dnd_subrace)
        assert subrace
        assert subrace.index == dnd_subrace["index"]
        assert subrace == subrace
        if prev_subrace is not None:
            assert prev_subrace != subrace
        prev_subrace = subrace
