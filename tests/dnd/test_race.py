from dnd.race import Race
import dnd.dndclient as dndclient


def test_init() -> None:

    prev_race = None
    for r in dndclient.get_races():
        dnd_race = dndclient.get_race(r["index"])
        race = Race(dnd_race)
        assert race
        assert race.index == dnd_race["index"]
        assert race == race
        if prev_race is not None:
            assert prev_race != race
        prev_race = race
