import requests

import dnd.dndclient as dndclient

# Amount of times to perform random tasks
N = 10


def test_globals() -> None:

    request = requests.get(dndclient.BASE_URL)
    assert request.status_code == 200

    assert len(dndclient.EXPERIENCE_REQUIREMENT) == 20
    assert len(dndclient.CURRENCY_RATES) > 0


def test_query() -> None:

    response = dndclient.query("/api")

    assert isinstance(response, dndclient.DNDResponse)
    assert len(response) > 0


def test_experience_level() -> None:

    assert dndclient.experience_level(0) == 1
    assert dndclient.experience_level(299) == 1
    assert dndclient.experience_level(300) == 2
    assert dndclient.experience_level(355001) == 20


def test_proficiency_ability() -> None:

    ability = dndclient.get_proficiency_ability("skill-athletics")
    assert isinstance(ability, dndclient.DNDResponse)
    assert len(ability) > 0

    ability = dndclient.get_proficiency_ability("alchemists-supplies")
    assert len(ability) == 0


def test_get_monsters() -> None:

    monsters = dndclient.get_monsters([0, 0.125, 0.25, 0.5])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dndclient.get_monster(m["index"])
        assert monster["challenge_rating"] in [0, 0.125, 0.25, 0.5]

    monsters = dndclient.get_monsters([1])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dndclient.get_monster(m["index"])
        assert monster["challenge_rating"] == 1

    monsters = dndclient.get_monsters([2, 5, 12])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dndclient.get_monster(m["index"])
        assert monster["challenge_rating"] in [2, 5, 12]


def test_get_monster_of_rating() -> None:

    for _ in range(N):
        monster = dndclient.get_monster_of_rating([0, 0.125, 0.25, 0.5])
        assert monster["challenge_rating"] in [0, 0.125, 0.25, 0.5]

        monster = dndclient.get_monster_of_rating([1])
        assert monster["challenge_rating"] == 1

        monster = dndclient.get_monster_of_rating([2, 3])
        assert monster["challenge_rating"] in [2, 3]


def test_get_monster_lower() -> None:

    for _ in range(N):
        monster = dndclient.get_monster_lower()
        assert monster["challenge_rating"] <= 0.5

        monster = dndclient.get_monster_lower(20)
        assert monster["challenge_rating"] <= 20

        monster = dndclient.get_monster_lower(0)
        assert monster["challenge_rating"] == 0

        monster = dndclient.get_monster_lower(0.3)
        assert monster["challenge_rating"] in [0, 0.125, 0.25]


def test_get_monster_image() -> None:

    monster = dndclient.get_monster("aboleth")
    assert len(dndclient.get_monster_image_link(monster)) > 0

    monster = dndclient.get_monster("deer")
    assert len(dndclient.get_monster_image_link(monster)) == 0


def test_xp_to_max_cr() -> None:

    for i in range(dndclient.EXPERIENCE_REQUIREMENT[-1] + 1):
        assert 0.25 <= dndclient.xp_to_max_cr(i) <= 30


def test_ability_modifier() -> None:

    assert dndclient.ability_modifier(10) == 0
    assert dndclient.ability_modifier(8) == -1
    assert dndclient.ability_modifier(12) == 1
    assert dndclient.ability_modifier(0) == -5
    assert dndclient.ability_modifier(20) == 5


def test_cr_proficiency() -> None:

    assert dndclient.cr_proficiency(0) == 2
    assert dndclient.cr_proficiency(0.125) == 2
    assert dndclient.cr_proficiency(4) == 2
    assert dndclient.cr_proficiency(5) == 3
    assert dndclient.cr_proficiency(28) == 8
    assert dndclient.cr_proficiency(29) == 9
    assert dndclient.cr_proficiency(30) == 9
