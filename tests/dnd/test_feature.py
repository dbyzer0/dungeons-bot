import dnd.dndclient as dndclient
from dnd.feature import Feature

def test_init() -> None:

    for f in dndclient.get_features():
        dnd_feature = dndclient.get_feature(f["index"])
        feature = Feature(dnd_feature)
        assert feature
        assert feature.index == dnd_feature["index"]


def test_hash() -> None:

    feature_set: set[Feature] = set()

    arcane_recovery = dndclient.get_feature("arcane-recovery")
    feature_set.add(Feature(arcane_recovery))
    assert len(feature_set) == 1

    arcane_tradition = dndclient.get_feature("arcane-tradition")
    feature_set.add(Feature(arcane_tradition))
    assert len(feature_set) == 2

    feature_set.add(Feature(arcane_recovery))
    assert len(feature_set) == 2
