from dnd.subclass import Subclass
import dnd.dndclient as dndclient


def test_init() -> None:

    prev_subclass = None
    for c in dndclient.get_subclasses():
        dnd_subclass = dndclient.get_subclass(c["index"])
        subclass = Subclass(dnd_subclass)
        assert subclass
        assert subclass.index == dnd_subclass["index"]
        assert subclass == subclass
        if prev_subclass is not None:
            assert prev_subclass != subclass
        prev_subclass = subclass
