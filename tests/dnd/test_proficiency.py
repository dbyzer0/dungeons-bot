import dnd.dndclient as dndclient
from dnd.proficiency import Proficiency


def test_init() -> None:

    prev_proficiency = None
    for p in dndclient.get_proficiencies():
        dnd_proficiency = dndclient.get_proficiency(p["index"])
        proficiency = Proficiency(dnd_proficiency)
        assert proficiency
        assert proficiency.index == dnd_proficiency["index"]
        assert proficiency == proficiency
        if prev_proficiency is not None:
            assert prev_proficiency != proficiency
        prev_proficiency = proficiency
