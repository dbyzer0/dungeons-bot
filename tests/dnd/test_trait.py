import dnd.dndclient as dndclient
from dnd.trait import Trait


def test_init() -> None:

    prev_trait = None
    for t in dndclient.get_traits():
        dnd_trait = dndclient.get_trait(t["index"])
        trait = Trait(dnd_trait)
        assert trait
        assert trait.index == dnd_trait["index"]
        assert trait == trait
        if prev_trait is not None:
            assert prev_trait != trait
        prev_trait = trait
