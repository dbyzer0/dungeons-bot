import pytest
from enums import Rarity
from dnd.magic_item import MagicItem, PotionOfFlying, PotionOfGiantStrength, PotionOfHealing, PotionOfHeroism
import dnd.dndclient as dndclient
from dice import Dice
from currency import Currency, Unit


def test_init() -> None:

    armor = dndclient.get_magic_item("adamantine-armor")
    item = MagicItem(armor)
    assert item
    assert item._dnd_dict["index"] == armor["index"]
    assert item.index == "adamantine-armor"
    assert item.name == "Adamantine Armor"
    assert item.equipment_category == "armor"
    assert item.rarity == Rarity.UNCOMMON
    assert item == item

    amulet = dndclient.get_magic_item("amulet-of-the-planes")
    item = MagicItem(amulet)
    assert item
    assert item._dnd_dict["index"] == amulet["index"]
    assert item.rarity == Rarity.VERY_RARE

    ammunition = dndclient.get_magic_item("ammunition")
    item = MagicItem(ammunition)
    assert item
    assert item._dnd_dict["index"] == ammunition["index"]
    assert item.rarity == Rarity.VARIES
    assert not item.variant
    assert len(item.variants) == 3

    ammunition_1 = dndclient.get_magic_item("ammunition-1")
    item = MagicItem(ammunition_1)
    assert item
    assert item._dnd_dict["index"] == ammunition_1["index"]
    assert item.rarity == Rarity.UNCOMMON
    assert item.variant
    assert len(item.variants) == 0


def test_potion_of_healing() -> None:

    potion = PotionOfHealing.common()
    assert potion
    assert potion.cost == Currency(50, Unit.gp)
    assert potion.healing == Dice("2d4+2")
    assert 4 <= potion.use() <= 10
    assert potion.use() == 0


def test_potion_of_giant_strength() -> None:

    potion = PotionOfGiantStrength.hill()
    assert potion
    assert potion.use() == 21
    assert potion.use() == 0


def test_potion_of_flying() -> None:

    potion = PotionOfFlying.inst()
    assert potion
    assert potion.use() == 1
    assert potion.use() == 0


def test_potion_of_heroism() -> None:

    potion = PotionOfHeroism.inst()
    assert potion
    use = potion.use()
    assert use.temp_hitpoints == 10
    assert 1 <= use.bless <= 4
    use = potion.use()
    assert use.temp_hitpoints == 0
    assert use.bless == 0
