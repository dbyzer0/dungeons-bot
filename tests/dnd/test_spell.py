import dnd.dndclient as dndclient
from dnd.spell import Spell


def test_init() -> None:

    prev_spell = None
    for s in dndclient.get_spells():
        dnd_spell = dndclient.get_spell(s["index"])
        spell = Spell(dnd_spell)
        assert spell
        assert spell == spell
        assert spell.index == dnd_spell["index"]
        if prev_spell is not None:
            assert spell != prev_spell
        prev_spell = spell
