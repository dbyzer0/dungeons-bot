import pytest
from currency import Currency
from item import Item

def test_init() -> None:

    item = Item("test", "Test", "t", Currency(5))
    assert item
    assert item.index == "test"
    assert item.name == "Test"
    assert item.emoji == "t"
    assert item.cost == Currency(5)


def test_nice_name() -> None:

    item = Item("test", "Test")
    assert item.nice_name() == "Test"
