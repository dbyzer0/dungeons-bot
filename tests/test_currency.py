import pytest

import dnd.dndclient as dndclient
from currency import Currency, Unit

def test_init() -> None:

    currency = Currency(10)
    assert currency
    assert currency.index == "gp"
    assert currency.name == "Gold"
    assert currency.unit == Unit.gp
    assert currency.amount == 10

    currency = Currency(5, Unit.sp)
    assert currency
    assert currency.index == "sp"
    assert currency.name == "Silver"
    assert currency.unit == Unit.sp
    assert currency.amount == 5


def test_combine() -> None:

    c1 = Currency(2, Unit.cp)
    c2 = Currency(3, Unit.sp)
    c3 = Currency(4, Unit.gp)
    c4 = Currency(5, Unit.pp)
    assert Currency.combine(c1) == c1
    assert Currency.combine(c1, c2) == Currency(3.2, Unit.sp)
    assert Currency.combine(c1, c2, c3) == Currency(4.32, Unit.gp)
    assert Currency.combine(c1, c2, c3, c4) == Currency(5.432, Unit.pp)
    assert Currency.combine(*[c1, c2, c3, c4]) == Currency(5.432, Unit.pp)


def test_from_dict() -> None:

    d = {
        "quantity": 5,
        "unit": "sp"
    }
    assert Currency.from_dict(d) == Currency(5, Unit.sp)


def test_amount() -> None:

    assert Currency(0, Unit.cp).amount == 0
    assert Currency(0.9, Unit.cp).amount == 0
    assert Currency(2.54321, Unit.cp).amount == 2
    assert Currency(2.54321, Unit.sp).amount == 2.5
    assert Currency(2.54321, Unit.ep).amount == 2.54
    assert Currency(2.54321, Unit.gp).amount == 2.54
    assert Currency(2.54321, Unit.pp).amount == 2.543


def test_to() -> None:

    currency = Currency(5, Unit.cp)
    assert currency.to(Unit.cp) == currency
    assert currency.to(Unit.sp) == Currency(0.5, Unit.sp)
    assert currency.to(Unit.ep) == Currency(0.1, Unit.ep)
    assert currency.to(Unit.gp) == Currency(0.05, Unit.gp)
    assert currency.to(Unit.pp) == Currency(0.005, Unit.pp)

    assert currency.to(Unit.cp, True) == currency
    assert currency.to(Unit.sp, True) == Currency(1, Unit.sp)
    assert currency.to(Unit.ep, True) == Currency(1, Unit.ep)
    assert currency.to(Unit.gp, True) == Currency(1, Unit.gp)
    assert currency.to(Unit.pp, True) == Currency(1, Unit.pp)

    currency = Currency(5, Unit.pp)
    assert currency.to(Unit.cp) == Currency(5000, Unit.cp)
    assert currency.to(Unit.sp) == Currency(500, Unit.sp)
    assert currency.to(Unit.ep) == Currency(100, Unit.ep)
    assert currency.to(Unit.gp) == Currency(50, Unit.gp)
    assert currency.to(Unit.pp) == currency

    assert currency.to(Unit.cp, True) == Currency(5000, Unit.cp)
    assert currency.to(Unit.sp, True) == Currency(500, Unit.sp)
    assert currency.to(Unit.ep, True) == Currency(100, Unit.ep)
    assert currency.to(Unit.gp, True) == Currency(50, Unit.gp)
    assert currency.to(Unit.pp, True) == currency

    currency = Currency(-5, Unit.gp)
    assert currency.to(Unit.cp) == Currency(-500, Unit.cp)
    assert currency.to(Unit.pp) == Currency(-0.5, Unit.pp)
    assert currency.to(Unit.cp, True) == Currency(-500, Unit.cp)
    assert currency.to(Unit.pp, True) == Currency(-1, Unit.pp)


def test_rounded() -> None:

    assert Currency(1.00, Unit.pp).rounded() == Currency(1, Unit.pp)
    assert Currency(1.01, Unit.pp).rounded() == Currency(2, Unit.pp)
    assert Currency(1.99, Unit.pp).rounded() == Currency(2, Unit.pp)


def test_reduce() -> None:

    assert Currency(1000, Unit.pp).reduce() == Currency(1000, Unit.pp)
    assert Currency(1000, Unit.gp).reduce() == Currency(100, Unit.pp)
    assert Currency(1000, Unit.sp).reduce() == Currency(10, Unit.pp)
    assert Currency(1000, Unit.cp).reduce() == Currency(1, Unit.pp)

    assert Currency(0.54321, Unit.pp).reduce() == Currency(5.43, Unit.gp)
    assert Currency(0.54321, Unit.gp).reduce() == Currency(5.4, Unit.sp)
    assert Currency(0.54321, Unit.sp).reduce() == Currency(5, Unit.cp)


def test_equivalent() -> None:

    assert Currency(5, Unit.cp).equivalent(Currency(5, Unit.cp))
    assert Currency(5, Unit.cp).equivalent(Currency(0.5, Unit.sp))
    assert Currency(5, Unit.cp).equivalent(Currency(0.1, Unit.ep))
    assert Currency(5, Unit.cp).equivalent(Currency(0.05, Unit.gp))
    assert Currency(5, Unit.cp).equivalent(Currency(0.005, Unit.pp))

    assert Currency(5, Unit.pp).equivalent(Currency(5000, Unit.cp))
    assert Currency(5, Unit.pp).equivalent(Currency(500, Unit.sp))
    assert Currency(5, Unit.pp).equivalent(Currency(100, Unit.ep))
    assert Currency(5, Unit.pp).equivalent(Currency(50, Unit.gp))
    assert Currency(5, Unit.pp).equivalent(Currency(5, Unit.pp))


def test_split() -> None:

    currency = Currency(0.432, Unit.pp)
    s = currency.split()
    assert s
    assert len(s) == 3
    assert s[0] == Currency(4, Unit.gp)
    assert s[1] == Currency(3, Unit.sp)
    assert s[2] == Currency(2, Unit.cp)

    currency = Currency(0, Unit.sp)
    s = currency.split()
    assert s
    assert len(s) == 1
    assert s[0] == Currency(0, Unit.sp)


def test_add() -> None:

    currency = Currency(0.5, Unit.sp)
    assert currency.add(Currency(0.1, Unit.sp)) == Currency(0.6, Unit.sp)
    assert currency.add(Currency(2.42, Unit.gp)) == Currency(24.7, Unit.sp)
    assert currency.add(Currency(2.421, Unit.pp)) == Currency(242.6, Unit.sp)
    assert currency.add(Currency(2, Unit.cp), True) == Currency(1.5, Unit.sp)


def test_sub() -> None:

    currency = Currency(0.5, Unit.pp)
    assert currency.sub(Currency(0.1, Unit.pp)) == Currency(0.4, Unit.pp)
    assert currency.sub(Currency(1, Unit.gp)) == Currency(0.4, Unit.pp)
    assert currency.sub(Currency(2, Unit.sp)) == Currency(0.48, Unit.pp)
    assert currency.sub(Currency(3, Unit.cp)) == Currency(0.497, Unit.pp)


def test_arithmetic() -> None:

    currency = Currency(10)

    currency + 5
    assert currency.amount == 10
    assert (currency + 5).amount == 15
    assert currency.amount == 10
    assert (currency - 5).amount == 5
    assert currency.amount == 10
    assert (currency * 5).amount == 50
    assert currency.amount == 10
    assert (currency / 5).amount == 2
    assert currency.amount == 10

    assert currency + Currency(5) == Currency(15)
    assert currency - Currency(5) == Currency(5)


def test_comparisons() -> None:

    c1 = Currency(5)
    c2 = Currency(4)
    c3 = Currency(4)
    assert c1 != c2
    assert c2 == c3
    assert c2 < c1
    assert c2 <= c1
    assert c1 > c2
    assert c1 >= c2

    gold = Currency(5)
    silver = Currency(49, Unit.sp)
    assert gold > silver
    assert gold >= silver
    assert silver < gold
    assert silver <= gold
    silver += 1
    assert not gold > silver
    assert gold >= silver
    assert not silver < gold
    assert silver <= gold
