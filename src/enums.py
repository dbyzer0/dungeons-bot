from enum import Enum


class State(Enum):
    END = "end"
    BUILD = "build"
    ADVENTURE = "adventure"
    FIGHT = "fight"
    TOWN = "town"
    SHOP = "shop"
    ADVENTURE_MONSTER = "adventure_monster"
    ADVENTURE_STRANGER = "adventure_stranger"
    ADVENTURE_CHEST = "adventure_chest"
    TRADER = "trader"
    TREASURE = "treasure"


class Rarity(Enum):

    VARIES = "varies"
    COMMON = "common"
    UNCOMMON = "uncommon"
    RARE = "rare"
    VERY_RARE = "very_rare"
    LEGENDARY = "legendary"
