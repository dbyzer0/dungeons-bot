from io import BytesIO
import os
import random
from time import sleep
import requests
from requests import Response
from PIL import Image
import util


API_URL = util.config["stablehorde_url"]

DEFAULT_AVATAR_PATH = os.path.join(util.root, "data/avatar.png")
OVERLAY_AVATAR_PATH = os.path.join(util.root, "data/avatar-overlay.png")
AI_AVATAR_PATH = os.path.join(util.root, "data/avatar-overlay-ai.png")
AI_AVATAR_ORIGINAL_PATH = os.path.join(util.root, "data/avatar-ai.png")

DEFAULT_HEADER_PATH = os.path.join(util.root, "data/header.png")
OVERLAY_HEADER_PATH = os.path.join(util.root, "data/header-overlay.png")
AI_HEADER_PATH = os.path.join(util.root, "data/header-overlay-ai.png")
AI_HEADER_ORIGINAL_PATH = os.path.join(util.root, "data/header-ai.png")

ABILITY_TO_ADJ = {
    "str": [
        "strong",
        "powerful",
        "sturdy",
        "herculean"
    ],
    "dex": [
        "dexterous",
        "agile",
        "nimble",
        "graceful",
        "quick"
    ],
    "con": [
        "hardy",
        "resilient",
        "tough",
        "stalwart"
    ],
    "int": [
        "intelligent",
        "brilliant",
        "knowledgeable",
        "ingenious",
        "intellectual"
    ],
    "wis": [
        "wise",
        "insightful",
        "perceptive",
        "prudent",
        "discerning"
    ],
    "cha": [
        "charismatic",
        "persuasive",
        "captivating",
        "charming",
        "eloquent"
    ]
}


def delete_generated() -> None:
    """Delete AI generated images from the filesystem, if present.

    Specifically, deletes the AI original and overlayed header and avatar images.
    """
    if os.path.exists(AI_AVATAR_ORIGINAL_PATH):
        os.remove(AI_AVATAR_ORIGINAL_PATH)
        util.debug(f"Deleted {AI_AVATAR_ORIGINAL_PATH}")
    if os.path.exists(AI_AVATAR_PATH):
        os.remove(AI_AVATAR_PATH)
        util.debug(f"Deleted {AI_AVATAR_PATH}")
    if os.path.exists(AI_HEADER_ORIGINAL_PATH):
        os.remove(AI_HEADER_ORIGINAL_PATH)
        util.debug(f"Deleted {AI_HEADER_ORIGINAL_PATH}")
    if os.path.exists(AI_HEADER_PATH):
        os.remove(AI_HEADER_PATH)
        util.debug(f"Deleted {AI_HEADER_PATH}")


def api_generate(payload: dict, headers: dict) -> Response | None:
    """Submit a generation to the Stable Horde API.

    `payload`: the request payload, containing configuration values for generation.
    `headers`: the headers for the request, containing the API key.

    Returns the API `Response`, or `None` if errors are encountered.
    """
    tries = 0
    req = None
    while (req is None or not req.ok) and tries < util.config["max_retries"]:
        tries += 1
        try:
            util.debug(f"Submitting request to generate image...")
            req = requests.post(f"{API_URL}/api/v2/generate/async", json=payload, headers=headers)
        except Exception as e:
            util.warn(f"Failed to submit request ({tries}/{util.config['max_retries']}): {e}")
            sleep(util.config["retry_delay"])

    return req


def api_status(id: str) -> Response | None:
    """Query the Stable Horde API for the status of a generation.

    `id`: ID of the generation.

    Returns the API `Response`, or `None` if errors are encountered.
    """
    tries = 0
    req = None
    while (req is None or not req.ok) and tries < util.config["max_retries"]:
        tries += 1
        try:
            util.debug(f"Querying generation status...")
            req = requests.get(f"{API_URL}/api/v2/generate/status/{id}")
            util.debug(f"Generation time remaining ~{req.json()['wait_time']}s")
        except Exception as e:
            util.warn(f"Failed to query status ({tries}/{util.config['max_retries']}): {e}")
            sleep(util.config["retry_delay"])

    return req


def generate_image(prompt: str, dimensions: tuple[int, int], seed: str) -> Image.Image | None:
    """Submit an image for generation from Stable Horde.

    `prompt`: the prompt to generate from.
    `dimensions`: the dimensions of the image.
    `seed`: the seed to use for the generation.

    Returns the resulting `Image` or `None` if errors are encountered.
    """
    headers = {
        "apikey": util.config["stablehorde_key"],
        "Client-Agent": f"dungeons-bot:{util.config['_version']}:https://mastodon.social/@dungeons",
    }
    payload = {
        "prompt": prompt,
        "params": {
            "sampler_name": "k_euler",
            "models": [
                "stable_diffusion",
            ],
            "guidance": 20,
            "cfg_scale": 7.5,
            "denoising_strength": 0.75,
            "width": dimensions[0],
            "height": dimensions[1],
            "seed": seed,
            "seed_variation": 1,
            "post_processing": [
                "GFPGAN",       # improves faces
                "CodeFormers",  # improves faces
            ],
            "karras": True,
            "tiling": False,
            "hires_fix": True,
            "clip_skip": 1,
            "facefixer_strength": 0.9,
            "steps": 40,
            "n": 1,
        },
        "nsfw": False,
        "trusted_workers": True,
        "slow_workers": True,
        "censor_nsfw": False,
    }

    util.debug(f"Generating with prompt: {payload['prompt']}")
    generate_req = api_generate(payload, headers)

    if generate_req is None or not generate_req.ok:
        util.error(f"Could not submit request: {generate_req.text}")
        return None

    req_id = generate_req.json()["id"]
    util.debug(f"Submitted request with ID {req_id}")

    done = False
    status = None
    while not done:
        status_req = api_status(req_id)

        if status_req is None or not status_req.ok:
            util.error(f"Could not query status: {status_req.text}")
            return None

        status = status_req.json()
        done = status["done"]
        if not done:
            sleep(util.config["retry_delay"])

    image_url = status["generations"][0]["img"]
    image_req = requests.get(image_url)
    image = Image.open(BytesIO(image_req.content))
    return image


def overlay_image(base: Image.Image, overlay: Image.Image, original_path: str, save_path: str) -> tuple[str, str] | None:
    """Overlays the `overlay` image on top of `base`, saving the result.

    `base`: the base (background/bottom) image.
    `overlay`: the image to overlay on top of `base`.
    `original_path`: the path to save the unmodified `base` to.
    `save_path`: the path to save the modified `base` to.

    Returns the paths of the saved files (original, overlayed), or `None` if errors are encountered.
    """
    tries = 0
    while tries < util.config["max_retries"]:
        tries += 1
        try:
            base.save(original_path)
            util.debug(f"Saved original AI image to {original_path}")
            base.paste(overlay, (0, 0), overlay)
            base.save(save_path)
            util.debug(f"Saved new avatar to {save_path}")
            return (original_path, save_path)
        except Exception as e:
            util.warn(f"Error overlaying image: {e}")
            sleep(util.config["retry_delay"])

    return None


def generate_avatar(character) -> tuple[str, str] | None:
    """Generates a new avatar image based on the given character.

    `character`: the `Character` to generate images from.
    """
    from character import Character
    character: Character = character
    map = {a: character.ability_score(a) for a in Character.ORDERED_ABILITY_INDEXES}
    max_stat = max(map, key=map.get)

    adjectives = []

    weapon = character.hands[0].name
    armor = character.armor[0].name if character.armor else ""
    adjectives.append(random.choice(ABILITY_TO_ADJ[max_stat]))
    if character.race.index == "dwarf":
        adjectives.append("bearded")

    prompt = f"Androgynous, gender-neutral, D&D-style, zoomed out portrait of the {', '.join(adjectives)} hero {character.title(True)}. " +\
        f"Their whole body is fully visible, their head is fully visible, they are alone, against a plain background. " +\
        f"They wield a {weapon}"
    if armor:
        prompt += f", they are wearing {armor}."

    if not os.path.isfile(OVERLAY_AVATAR_PATH):
        util.warn(f"No overlay image provided at {OVERLAY_AVATAR_PATH}")
        util.warn("No avatar AI image will be generated")
        return None

    # Mastodon will scale to 400x400, but our dimensions must be multiples of 16
    generated_image = generate_image(prompt, (512, 512), character.title())
    if generated_image is None:
        return None

    overlay_result = overlay_image(generated_image, Image.open(OVERLAY_AVATAR_PATH), AI_AVATAR_ORIGINAL_PATH, AI_AVATAR_PATH)
    if overlay_result is None:
        return None

    return overlay_result


def generate_header(character) -> tuple[str, str] | None:
    """Generates a new header image based on the given character.

    `character`: the `Character` to generate images from.
    """
    from character import Character
    character: Character = character
    map = {a: character.ability_score(a) for a in Character.ORDERED_ABILITY_INDEXES}
    max_stat = max(map, key=map.get)

    adjectives = []

    weapon = character.hands[0].name
    armor = character.armor[0].name if character.armor else ""
    adjectives.append(random.choice(ABILITY_TO_ADJ[max_stat]))
    if character.race.index == "dwarf":
        adjectives.append("bearded")

    prompt = f"Androgynous, gender-neutral, D&D-style, zoomed out scene of character {character.title(True)} amidst a vast landscape. " +\
        f"They are alone, their whole body is fully visible, their head is fully visible. " +\
        f"They are {', '.join(adjectives)}, they wield a {weapon}"
    if armor:
        prompt += f", they are wearing {armor}."

    if not os.path.isfile(OVERLAY_HEADER_PATH):
        util.warn(f"No overlay image provided at {OVERLAY_HEADER_PATH}")
        util.warn("No header AI image will be generated")
        return None

    # Mastodon will scale to 1500x500, but our dimensions must be a multiple of 16 with a max of 1024
    generated_image = generate_image(prompt, (1024, 384), character.title())
    if generated_image is None:
        return None

    overlay_result = overlay_image(generated_image, Image.open(OVERLAY_HEADER_PATH), AI_HEADER_ORIGINAL_PATH, AI_HEADER_PATH)
    if overlay_result is None:
        return None

    return overlay_result
