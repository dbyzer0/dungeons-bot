from datetime import datetime, timezone
from enum import Enum
import math
import os
import pickle
import random
import re
from typing import Callable
from attacks import AttackResult, AttackSpell, AttackSummary

from currency import Currency, Unit
import client
import db
from dice import Dice
import dnd.dndclient as dndclient
from character import Character
from effect import EffectType
from enums import State
from dnd.equipment import Equipment
import image
import lang
from dnd.monster import Monster
import util
from item import Item
from dnd.magic_item import RARITY_WEIGHTS, MagicItem, PotionOfFlying, PotionOfHealing, PotionOfGiantStrength, PotionOfHeroism, PotionOfInvisibility, PotionOfResistance, PotionOfSpeed


class Option():

    def __init__(self, text: str, action: Callable) -> None:

        self.text = text
        self.action = action


    def __eq__(self, __value: object) -> bool:

        if isinstance(__value, Option):
            return self.text == __value.text and self.action == __value.action
        return False


# TODO: the stretch goal is for each of the functions in this class
# to be separated into node types that can be populated with behavior
# via a toml configuration
class Campaign:
    """A single Campaign, spanning the lifetime of a single character.
    A new Campaign is created upon character death.
    """

    def __init__(self, campaign_id: int = 0, version: str = "0.0.0") -> None:
        """Create a new Campaign.

        `campaign_id`: campaign ID.
        `version`: version string this campaign uses.
        """
        self.id: int = campaign_id
        self.character: Character = None
        self.monster: Monster = None
        self.start_time: datetime = None
        self.end_time: datetime = None
        self.votes = 0
        self.state: State = State.BUILD
        self.map = []
        self.version = version

        self.root_post_url = ""
        self.completed_post_url = ""
        self.current_post_url = ""

        self.root_post_id: int = None
        self.completed_post_id: int = None
        self.current_post_id: int = None

        self.last_result = ""


    def connect_handlers(self) -> int:
        """Connect event handlers.
        """
        if not client.dry:
            return client.listener.add_handler(self.command_handler)


    def save(self) -> None:
        """Save Campaign object to file.
        """
        SAVE_PATH = os.path.join(util.root, "data/save")
        with open(SAVE_PATH, "wb") as file:
            pickle.dump(
                {
                    "campaign": self,
                    "status_chain": client.status_chain
                },
                file
            )
        db.update_campaign(self)


    @staticmethod
    def load() -> "Campaign":
        """Load Campaign object from file.
        """
        with open(os.path.join(util.root, "data/save"), "rb") as file:
            data = pickle.load(file)
            # Temporarily account for old save methods
            if isinstance(data, Campaign):
                return data

            campaign: Campaign = None
            for key in data:
                match key:
                    case "campaign":
                        campaign = data[key]
                    case "status_chain":
                        client.status_chain = data[key]
                    case _:
                        util.error(f"Unexpected data key: {key}")

            return campaign


    def map_append(self, emoji: str) -> None:
        """Append an emoji to the map.
        """
        self.map.append(emoji)
        while len(self.map) > 10:
            self.map.pop(0)


    def map_str(self) -> str:
        """Get the map as a str.
        """
        return "".join(self.map)


    def poll(
            self,
            text: str,
            last_result: str,
            options: list[str],
            chain = True,
            option_groups: list = None,
            multiple = 1,
            dry_skip: list[int] = None
        ) -> list[int]:
        """Post a poll and get the result. Returns index in `options` that wins.

        `text`: the body of the poll.
        `last_result`: the last result of the previous poll (prepended to `text`)
        `options`: poll options.
        `chain`: whether to reply to the previous poll or not.
        `option_groups`: option groupings for winner determination.
        `multiple`: how many options that can be chosen
        `dry_skip`: list of indices in `options` to not choose if running `--dry`.
        """
        is_multiple = multiple > 1
        status = client.post_poll(text, last_result, options, chain, is_multiple=is_multiple)
        post_id = db.create_post(
            status["created_at"],
            text,
            last_result,
            options,
            option_groups,
            is_multiple,
            status["url"],
            status["id"],
            self.completed_post_id
        )
        if self.completed_post_id is not None:
            db.update_post(self.completed_post_id, child_id=post_id)

        if not self.root_post_url:
            self.root_post_url = status["url"]
        if self.root_post_id is None:
            self.root_post_id = post_id

        self.current_post_url = status["url"]
        self.current_post_id = post_id

        if self.character:
            db.update_campaign(self)

        result = client.poll_result(status["id"], chain, option_groups, dry_skip, multiple=multiple)
        while result is None:
            util.debug("Poll has no votes, reposting...")
            status = client.post_poll(text, last_result, options, chain, is_multiple=is_multiple, extend=True)
            self.current_post_url = status["url"]
            db.update_post(post_id, status_id=status["id"], url=status["url"])
            result = client.poll_result(status["id"], chain, option_groups, dry_skip, multiple=multiple)

        result_status = result[2]
        db.update_post(post_id, status_id=result_status["id"], url=result_status["url"])
        self.current_post_url = status["url"] # roll_result might change status, so we need to update this too
        self.completed_post_url = status["url"]
        self.completed_post_id = post_id
        self.votes += result[1]
        return result[0]


    def start(self, initial_text = "") -> None:
        """Start the Campaign loop.

        `result`: the message string of the last poll result.
        """
        util.info(f"STARTING CAMPAIGN #{self.id}")

        STATE_MAP = {
            State.BUILD: self.build,
            State.ADVENTURE: self.adventure,
            State.FIGHT: self.fight,
            State.TOWN: self.town,
            State.SHOP: self.shop,
            State.TRADER: self.trader,
            State.TREASURE: self.treasure
        }

        if initial_text:
            self.last_result = f"{initial_text}\n{self.last_result}"

        while self.state != State.END:

            self.last_result = STATE_MAP[self.state]()

            if self.character:
                client.update_profile(self.bio())

            util.info(self.last_result)
            self.save()

        self.end()
        self.save()

#region states

    def build(self) -> str:
        """Build a new Character.
        """
        # Generate character options
        util.info("Creating character options")
        options: list[Option] = []
        followers = list(client.get_followers())
        followers_copy = followers.copy()
        classes = dndclient.get_classes()
        races = dndclient.get_races()
        subclasses = dndclient.get_subclasses()
        subraces = dndclient.get_subraces()
        c_max = max([len(c["name"]) for c in classes])
        sc_max = max([len(sc["name"]) for sc in subclasses])
        r_max = max([len(r["name"]) for r in races])
        sr_max = max([len(sr["name"]) for sr in subraces])
        max_name_length = client.POLL_OPTION_LIMIT - max(c_max, sc_max) - max(r_max, sr_max) - 11

        for _ in range(client.MAX_POLL_SIZE):
            name = ""
            # choose a unique random name from follower list if available
            while len(name) < 2 and followers:
                choice = random.choice(followers)
                followers.remove(choice)
                name = choice.split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            if not name:
                name = random.choice(followers_copy).split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            name = name[:max_name_length]

            # try to have unique classes/races
            race = None
            clazz = None

            if races:
                race = random.choice(races)
                races.remove(race)
                race = dndclient.get_race(race["index"])

            if classes:
                clazz = random.choice(classes)
                classes.remove(clazz)
                clazz = dndclient.get_class(clazz["index"])

            options.append(self.option_character(Character(name, race, clazz)))

        # Poll
        util.info("Polling for character selection...")

        prompt = f"{util.emojis['campaign']['id']} Campaign {self.id} {client.HASHTAG_CAMPAIGN_START}\n\n"
        prompt += lang.create.prompt.choose()

        client.reset_avatar()
        client.reset_header()
        image.delete_generated()
        result = self.poll(prompt, self.last_result, [o.text for o in options], False)
        return options[result[0]].action()


    def fight(self) -> str:
        """Fight a Monster.
        """
        util.info(f"Fighting {self.monster.name}")

        attack_options = self.character.attack_options(client.MAX_POLL_SIZE - 1, self.monster)
        options: list[Option] = [self.option_attack_option(a) for a in attack_options]
        options.append(self.option_run(self.monster))

        result = self.poll(self.monster.bio() + f"\n{self.monster.image_url}", self.last_result, [o.text for o in options])
        return options[result[0]].action()


    def trader(self) -> str:
        """Stranger interaction turns out to be a wandering trader.
        """
        # We don't care about fair spread for the trader like we do for the
        # shop, so just give a random assortment of items.
        weapon_options = self.character.proficient_weapons()
        armor_options = self.character.proficient_armor()
        item_options: list[Item] = weapon_options + armor_options
        options: list[Option] = []

        if self.character.hitpoints < self.character.max_hitpoints:
            item_options += PotionOfHealing.all()
        item_options += PotionOfGiantStrength.all()
        item_options += PotionOfFlying.all()
        item_options += PotionOfHeroism.all()
        item_options += PotionOfInvisibility.all()
        item_options += PotionOfResistance.all()
        item_options += PotionOfSpeed.all()

        skill_check = self.character.skill_check("persuasion")
        buy_scale = 1.0 - (0.25 * util.clamp(skill_check / 20, 0, 1)) # up to 25% off

        trader_size = client.MAX_POLL_SIZE - 1

        possible_options = []
        while len(item_options) > 0:
            item = random.choice(item_options)
            item_options.remove(item)
            if (item.cost * buy_scale).rounded() <= self.character.currency:
                possible_options.append(self.option_buy_item(item, buy_scale=buy_scale))

        if self.character.currency.amount >= 100:
            for hand in self.character.hands:
                if hand.silvered: continue
                possible_options.append(self.option_silver(hand))
                break

        while trader_size > 0 and len(possible_options) > 1:
            option = random.choice(possible_options)
            possible_options.remove(option)
            options.append(option)
            trader_size -= 1

        if not options:
            self.state = State.ADVENTURE
            return lang.trader.result.empty.choose()

        options.append(self.option_leave())

        current = util.emojis["equipment"]["inventory"]
        current += f"\n{self.character.hand_str()}"
        current += f" {self.character.armor_str()}"
        result = self.poll(f"{lang.trader.prompt.choose()}\n{current}", self.last_result, [o.text for o in options])
        return options[result[0]].action()


    def treasure(self) -> str:
        """Interact with a treasure chest.
        """
        options: list[Option] = []

        possible_potions: list[MagicItem] = []
        if self.character.hitpoints < self.character.max_hitpoints:
            possible_potions.extend(PotionOfHealing.all())
        possible_potions.extend(PotionOfGiantStrength.all())
        possible_potions.extend(PotionOfFlying.all())
        possible_potions.extend(PotionOfHeroism.all())
        possible_potions.extend(PotionOfInvisibility.all())
        possible_potions.extend(PotionOfResistance.all())
        possible_potions.extend(PotionOfSpeed.all())
        weights = [RARITY_WEIGHTS[p.rarity] for p in possible_potions]

        roll = Dice(self.character.skill_check("investigation"), 8)
        options.append(self.option_take_item(Currency(roll.roll(), Unit.gp)))

        for _ in range(1, client.MAX_POLL_SIZE - 1):
            if len(possible_potions) == 0: continue
            potion = random.choices(possible_potions, weights=weights, k=1)[0]
            i = possible_potions.index(potion)
            possible_potions.pop(i)
            weights.pop(i)
            options.append(self.option_take_item(potion))

        skill_check = self.character.skill_check("investigation")
        treasure_count = max(1, round((min(20, skill_check) / 20) * len(options)))
        options.append(self.option_leave())
        leave_index = len(options) - 1

        result = self.poll(lang.treasure.prompt.choose(treasure_count), self.last_result, [o.text for o in options], multiple=treasure_count)

        # if the highest voted option was leave, skip all others
        if result[0] == leave_index:
            return options[result[0]].action()

        # otherwise, take until we hit leave or end of queue
        action_results = []
        for winner in result:
            if winner == leave_index:
                return "\n".join(action_results)
            action_results.append(options[winner].action())

        return "\n".join(action_results)


    def adventure(self) -> str:
        """Adventure beyond safety and explore.
        """
        max_monster_cr = dndclient.xp_to_max_cr(self.character.xp)
        monster = None
        mimic = Monster(dndclient.get_monster("mimic"))
        prompt = ""
        interaction = random.random()

        option_groups = []
        options: list[Option] = []

        can_rest = self.character.hit_dice > 0\
            or self.character.has_feature("pact-magic")\
            or self.character.has_feature("arcane-recovery")

        chest_chance = float(util.config["treasure_chance"])
        if mimic.challenge_rating > max_monster_cr:
            chest_chance /= 2
        stranger_chance = float(util.config["trader_chance"])
        util.debug(f"Choosing interaction: {interaction} {chest_chance} {stranger_chance}")

        # Chest
        if interaction >= 1.0 - chest_chance:
            mimic_chance = 0.5
            if mimic.challenge_rating > max_monster_cr:
                mimic_chance = 0
            mimic_skill_check = mimic.skill_check("stealth")
            character_skill_check = self.character.skill_check("perception")

            if random.random() <= mimic_chance:
                monster = mimic

            prompt = lang.chest.prompt.choose()
            if character_skill_check >= mimic_skill_check:
                # mimic
                if monster:
                    prompt = lang.chest.mimic.prompt.choose()
                # treasure
                else:
                    prompt = lang.chest.treasure.prompt.choose()

            self.state = State.ADVENTURE_CHEST
            option_groups = [[0, 1], [2, 3]]
            options += [
                self.option_attack(monster),
                self.option_investigate(monster)
            ]
            if not can_rest:
                options.append(self.option_avoid(None, "chest"))

        # Stranger
        # TODO: base stranger chance on gold amount instead of just bandit
        elif interaction >= 1.0 - chest_chance - stranger_chance:
            # https://www.desmos.com/calculator/mssjrefci6
            gold_threshold = self.character.GOLD_ROLL.max() - self.character.GOLD_ROLL.mean()
            bandit_chance = (math.atan(max(self.character.currency.amount - gold_threshold, 0) / 100) / (0.8 * math.pi)) + 0.2
            bandit_skill_check = "persuasion"
            spawn_bandit = random.random() <= bandit_chance

            # spawn bandit
            if spawn_bandit:
                bandit_captain = Monster(dndclient.get_monster("bandit"))
                bandit = Monster(dndclient.get_monster("bandit"))
                monster = bandit
                if bandit_captain.challenge_rating <= max_monster_cr:
                    monster = random.choice([bandit, bandit_captain])
                bandit_skill_check = "deception"

            # spawn trader
            else:
                monster = Monster(dndclient.get_monster("commoner"))

            character_check = self.character.skill_check("perception")
            monster_check = monster.skill_check(bandit_skill_check)

            prompt = lang.stranger.prompt.choose()
            if character_check >= monster_check:
                if spawn_bandit:
                    prompt = lang.stranger.bandit.prompt.choose()
                else:
                    prompt = lang.stranger.trader.prompt.choose()

            self.state = State.ADVENTURE_STRANGER
            option_groups = [[0, 1], [2, 3]]
            options += [
                self.option_attack(monster),
                self.option_talk(monster)
            ]
            if not can_rest:
                options.append(self.option_avoid(None, "stranger"))

        # Fight
        else:
            monster = Monster(dndclient.get_monster_lower(max_monster_cr))
            prompt = lang.fight.prompt.choose(monster.short_bio()) + f"\n{monster.image_url}"
            option_groups = [[0], [1, 2]]
            if can_rest:
                option_groups = [[0], [[1], [2, 3]]]
            self.state = State.ADVENTURE_MONSTER
            options += [
                self.option_fight(monster),
                self.option_avoid(monster)
            ]

        if can_rest:
            options.append(self.option_short_rest())

        options.append(self.option_town())

        result = self.poll(prompt, self.last_result, [o.text for o in options], option_groups=option_groups)
        return options[result[0]].action()


    def town(self) -> str:
        """Rest in a town or shop.
        """
        options = [
            self.option_adventure(),
            self.option_shop(),
            self.option_long_rest(),
            self.option_retire()
        ]

        result = self.poll(lang.town.prompt.choose(), self.last_result, [o.text for o in options], option_groups=[[0, 1, 2]], dry_skip=[3])
        return options[result[0]].action()


    def shop(self) -> str:
        """Shop for new equipment.
        """
        options: list[Option] = []
        weapon_options = self.character.proficient_weapons()
        armor_options = self.character.proficient_armor()

        sell_size = int(
            (self.character.has_feature("barbarian-unarmored-defense")
            or self.character.has_feature("monk-unarmed-defense"))
            and len(self.character.armor) > 0
        )
        shop_size = client.MAX_POLL_SIZE - sell_size - 1
        weapon_count = 0
        if not armor_options:
            weapon_count = min(len(weapon_options), shop_size)
        else:
            weapon_count = min(len(weapon_options), random.randint(1, math.ceil(shop_size / 2)))
        armor_count = min(len(armor_options), shop_size - weapon_count)

        for _ in range(weapon_count):
            weapon = None
            cost = 0
            while not weapon and weapon_options:
                weapon = random.choice(weapon_options)
                weapon_options.remove(weapon)
                unequipped = self.character.equip_equipment(weapon, True, True)
                cost = Equipment.net_spend(unequipped, [weapon]).rounded()
                if cost > self.character.currency:
                    weapon = None

            if weapon:
                options.append(self.option_buy_item(weapon))

        for _ in range(armor_count):
            armor = None
            cost = 0
            while (not armor or armor == self.character.armor) and armor_options:
                armor = random.choice(armor_options)
                armor_options.remove(armor)
                unequipped = self.character.equip_equipment(armor, True, True)
                cost = Equipment.net_spend(unequipped, [armor]).rounded()
                can_carry = armor.str_minimum <= self.character.abilities["str"]
                if cost > self.character.currency or not can_carry:
                    armor = None

            if armor:
                options.append(self.option_buy_item(armor))

        if sell_size > 0:
            options.append(self.option_sell_armor())

        if not options:
            self.state = State.TOWN
            return lang.shop.result.empty.choose()

        options.append(self.option_leave())

        current = util.emojis["equipment"]["inventory"]
        current += f"\n{self.character.hand_str()}"
        current += f" {self.character.armor_str()}"

        result = self.poll(f"{lang.shop.prompt.choose()}\n{current}", self.last_result, [o.text for o in options])
        return options[result[0]].action()


    def end(self) -> None:
        """End the campaign, resulting in a death message.
        """
        util.info("Campaign ended.")
        self.map_append(util.emojis["health"]["death"])
        NOW = datetime.now(timezone.utc)
        self._ended_at = NOW

        if not self.character:
            return

        self.character.died_at = NOW

        delta = NOW - self.start_time
        bio = "{}\n\n{}\n{} {} {} {} {} {}\n{}".format(
            self.character.short_bio(),
            self.map_str(),
            util.emojis["campaign"]["id"],
            self.id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds()),
            util.emojis["campaign"]["user"],
            self.votes,
            client.HASHTAG_CAMPAIGN_END
        )

        status = client.post_status(bio, self.last_result, image_path=image.AI_AVATAR_ORIGINAL_PATH, image_alt="AI-generated Character avatar")
        post_id = db.create_post(status["created_at"], bio, self.last_result, [], None, False, status["url"], status["id"], self.completed_post_id)
        db.update_post(self.completed_post_id, child_id=post_id)

#endregion
#region options

    def option_character(self, character: Character) -> Option:

        def action() -> str:
            self.character = character
            self.character.init()

            self.start_time = datetime.now(timezone.utc)

            if util.config["start_exp"] > 0:
                self.character.add_experience(util.config["start_exp"])
                self.character.long_rest()
            client.update_profile(self.bio())

            campaign_id, character_id = db.create_campaign(self, self.character)
            if campaign_id != 0:
                self.id = campaign_id
            if character_id != 0:
                self.character.id = character_id
            else:
                self.character.id = self.id
            client.start_image_gen(self.character)

            self.map_append(self.character.emoji())
            self.state = State.TOWN
            return lang.create.result.choose(character.name)

        return Option(
            lang.options.character.choose(
                character.emoji(), character.title()
            ),
            action
        )


    def option_adventure(self) -> Option:

        def action() -> str:
            self.state = State.ADVENTURE
            return lang.adventure.result.choose()

        return Option(
            lang.options.adventure.choose(
                util.emojis["prompts"]["adventure"]
            ),
            action
        )


    def option_buy_item(self, item: Item, buy_scale = 1.0, sell_scale = 0.5) -> Option:

        cost = (item.cost * buy_scale).rounded()
        if isinstance(item, Equipment):
            cost = Equipment.net_spend(
                self.character.equip_equipment(item, True, True),
                [item],
                buy_scale,
                sell_scale
            ).rounded()

        def action() -> str:
            if not isinstance(item, Currency):
                self.character.spend(cost)
            equip_result = self.character.equip_item(item, True)

            match self.state:
                case State.SHOP:
                    self.state = State.TOWN
                    return lang.purchase.result.choose(item.nice_name())

                case State.TRADER:
                    self.state = State.ADVENTURE
                    match item:
                        case Equipment():
                            return lang.trader.result.choose(
                                item.nice_name()
                            )

                        case PotionOfHealing()\
                                | PotionOfGiantStrength()\
                                | PotionOfFlying()\
                                | PotionOfHeroism()\
                                | PotionOfInvisibility()\
                                | PotionOfResistance()\
                                | PotionOfSpeed():
                            return lang.trader.result.effect.choose(
                                item.nice_name(),
                                item.effect_str
                            )

                        case _:
                            raise ValueError(f"Invalid item {item}")

                case _:
                    raise ValueError("Invalid state " + self.state)

        text = ""
        match item:
            case Equipment():
                if item.is_weapon():
                    text = "{} {} [{}] ~ {} ({:+} {})".format(
                        item.get_emoji(),
                        item.nice_name(),
                        self.character.weapon_dice_str(item, item.damage_dice),
                        " ".join(item.get_property_emojis()),
                        -int(cost.amount),
                        util.emojis["equipment"]["gold"]
                    )
                elif item.is_armor():
                    ac = f"[{self.character.armor_class([item])}]"
                    if item.is_shield():
                        ac = "[+{}] ~ {}".format(
                            item.armor_class,
                            util.emojis["weapon_properties"]["one-handed"]
                        )
                    text = "{} {} {} ({:+} {})".format(
                        item.get_emoji(),
                        item.nice_name(),
                        ac,
                        -int(cost.amount),
                        util.emojis["equipment"]["gold"]
                    )

            case MagicItem():
                text = "{} {} ({:+} {})".format(
                    item.emoji,
                    item.name,
                    -int(cost.amount),
                    cost.emoji
                )

        return Option(
            lang.options.buy_item.choose(
                text
            ),
            action
        )


    def option_take_item(self, item: Item) -> Option:

        def action():
            equip_result = self.character.equip_item(item)
            self.state = State.ADVENTURE

            match item:
                case Currency():
                    return lang.treasure.result.choose(f"{int(item.amount)} GP")

                case Equipment():
                    return lang.treasure.result.choose(f"{item.nice_name()}")

                case PotionOfHealing()\
                        | PotionOfGiantStrength()\
                        | PotionOfFlying()\
                        | PotionOfHeroism()\
                        | PotionOfInvisibility()\
                        | PotionOfResistance()\
                        | PotionOfSpeed():
                    return lang.treasure.result.effect.choose(
                        item.nice_name(),
                        item.effect_str
                    )

                case _:
                    raise TypeError(f"Invalid Item type {type(item)}.")

        text = ""
        match item:
            case Currency():
                text = lang.options.take_item.choose(
                    util.emojis["equipment"]["gold"],
                    f"{int(item.amount)} GP"
                )

            case MagicItem():
                text = lang.options.take_item.choose(
                    util.emojis["equipment"]["potion"],
                    item.nice_name()
                )

            case _:
                raise TypeError(f"Invalid Item type {type(item)}.")

        return Option(text, action)


    def option_silver(self, equipment: Equipment) -> Option:

        SILVER_COST = Currency(100)

        def action():

            for hand in self.character.hands:
                if hand.silvered: continue
                self.character.spend(SILVER_COST)
                hand.set_silvered(True)
                self.state = State.ADVENTURE
                return lang.trader.result.silver.choose(hand.nice_name())

        return Option(
            lang.options.silver.choose(
                util.emojis["equipment"]["silver"],
                "{} {}".format(-int(SILVER_COST.amount), util.emojis["equipment"]["gold"])
            ),
            action
        )


    def option_sell_armor(self, sell_scale = 0.5) -> Option:

        current_armor = self.character.armor
        result_ac = self.character.armor_class([])
        net_gold = Equipment.net_spend(current_armor, [], sell_scale=sell_scale)

        def action():
            self.state = State.TOWN
            self.character.armor = []
            self.character.spend(net_gold)
            return lang.sell.result.armor.choose(
                -int(net_gold.amount),
                util.emojis["equipment"]["gold"]
            )

        return Option(
            lang.options.sell_armor.choose(
                util.emojis["equipment"]["money"],
                result_ac,
                "{:+}".format(-int(net_gold.amount)),
                util.emojis["equipment"]["gold"]
            ),
            action
        )


    def option_shop(self) -> Option:

        def action() -> str:
            self.state = State.SHOP
            return lang.shop.result.choose()

        return Option(
            lang.options.shop.choose(
                util.emojis["prompts"]["shop"]
            ),
            action
        )


    def option_short_rest(self) -> Option:

        def action() -> str:
            hit_dice = self.character.hit_dice
            short_rest_result = self.character.short_rest()
            self.state = State.ADVENTURE

            result_strs = []

            if hit_dice > 0:
                result_strs.append(
                    lang.rest_short.result.choose(short_rest_result.hitpoints_gained)
                )
            else:
                result_strs.append(lang.rest_short.result.no_hit_dice.choose())

            if len(short_rest_result.effects_lost) > 0:
                result_strs.append(
                    lang.rest_short.result.effects.choose(len(short_rest_result.effects_lost))
                )

            return "\n".join(result_strs)

        return Option(
            lang.options.short_rest.choose(
                util.emojis["prompts"]["short_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            action
        )


    def option_long_rest(self) -> Option:

        def action() -> str:
            long_rest_result = self.character.long_rest()
            self.state = State.TOWN
            result_strs = [
                lang.rest_long.result.choose(
                    long_rest_result.hitpoints_gained,
                    long_rest_result.hitdice_gained
                )
            ]
            if len(long_rest_result.effects_lost) > 0:
                result_strs.append(
                    lang.rest_long.result.effects.choose(
                        len(long_rest_result.effects_lost)
                    )
                )

            return "\n".join(result_strs)

        return Option(
            lang.options.long_rest.choose(
                util.emojis["prompts"]["long_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            action
        )


    def option_retire(self) -> Option:

        def action() -> str:
            self.state = State.END
            return lang.retire.choose()

        return Option(
            lang.options.retire.choose(
                util.emojis["prompts"]["retire"]
            ),
            action
        )


    def option_town(self, text: str = None) -> Option:

        def action() -> str:
            if self.state == State.SHOP:
                self.state = State.TOWN
                return lang.shop.result.left.choose()
            else:
                self.map_append(util.emojis["prompts"]["town"])
                self.state = State.TOWN
                return lang.town.result.choose()

        if text is None:
            text = lang.options.town.choose(
                util.emojis["prompts"]["town"]
            )
        return Option(text, action)


    def option_fight(self, monster: Monster) -> Option:

        def action() -> str:
            mi = monster.roll_initiative()
            ci = self.character.roll_initiative()
            util.debug(f"MI: {mi}, CI: {ci}")
            self.monster = monster
            self.character.recent_rolls["Initiative"] = ci
            self.monster.recent_rolls["Initiative"] = mi
            self.monster.id = db.create_monster(self.monster, self)
            self.map_append(monster.emoji())
            self.state = State.FIGHT
            return lang.fight.result.choose(monster.name)

        return Option(
            lang.options.fight.choose(
                util.emojis["prompts"]["fight"]
            ),
            action
        )


    def option_attack(self, monster: Monster) -> Option:
        """Attack a monster. Attack is different than Fight because it is
        not necessarily prompted, while a Fight is mutual.
        """
        def action() -> str:
            # Chest
            if self.state == State.ADVENTURE_CHEST:
                # mimic
                if monster and "mimic" in monster.index:
                    self.option_fight(monster).action()
                    self.monster.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.chest.mimic.result.attack.choose()
                # treasure
                else:
                    self.state = State.ADVENTURE
                    return lang.chest.treasure.result.attack.choose()

            # Stranger
            elif self.state == State.ADVENTURE_STRANGER:
                # bandit
                if "bandit" in monster.index:
                    self.option_fight(monster).action()
                    self.monster.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.stranger.bandit.result.attack.choose()
                # trader
                else:
                    self.state = State.ADVENTURE
                    return lang.stranger.trader.result.attack.choose()

            else:
                raise ValueError("Invalid parent state " + self.state)

        return Option(
            lang.options.attack.choose(
                util.emojis["prompts"]["fight"]
            ),
            action
        )


    def option_talk(self, monster: Monster) -> Option:

        def action() -> str:
            # Stranger
            if self.state == State.ADVENTURE_STRANGER:
                # bandit
                if "bandit" in monster.index:
                    self.option_fight(monster).action()
                    self.character.set_surprised()
                    self.map_append(monster.emoji())
                    return lang.stranger.bandit.result.talk.choose()
                # trader
                else:
                    self.map_append(monster.emoji())
                    self.state = State.TRADER
                    return lang.stranger.trader.result.talk.choose()

            else:
                raise ValueError("Invalid parent state " + self.state)

        return Option(
            lang.options.talk.choose(
                util.emojis["skills"]["persuasion"]
            ),
            action
        )


    def option_investigate(self, monster: Monster) -> Option:

        def action() -> str:
            if monster and "mimic" in monster.index:
                self.state = State.FIGHT
                self.option_fight(monster).action()
                self.character.set_surprised()
                self.map_append(monster.emoji())
                return lang.chest.mimic.result.investigate.choose()
            else:
                self.map_append(util.emojis["equipment"]["chest"])
                self.state = State.TREASURE
                return lang.chest.treasure.result.investigate.choose()

        return Option(
            lang.options.investigate.choose(
                util.emojis["skills"]["perception"]
            ),
            action
        )


    def option_attack_option(self, attack_summary: AttackSummary) -> Option:

        def action() -> str:
            attack_summaries = []
            # order attacks
            creatures = (self.character, self.monster)
            while creatures[0].get_initiative() == creatures[1].get_initiative():
                creatures[0].roll_initiative()
                creatures[1].roll_initiative()
            creatures = tuple(sorted(creatures, key=lambda c: c.get_initiative(), reverse=True))
            util.debug(f"Combat order: {creatures}")

            # nullify surprise if both creatures are surprised
            # https://i.kym-cdn.com/entries/icons/original/000/023/397/C-658VsXoAo3ovC.jpg
            if all(c.get_surprised() for c in creatures):
                [c.set_surprised(False) for c in creatures]

            # roll in pairs until someone hits
            attack_results: list[AttackResult] = []
            while not attack_results or all(a.attack_hit.value <= 0 for a in attack_results):
                attack_results.clear()
                # character attacks first
                if isinstance(creatures[0], Character):
                    attack_results.append(creatures[0].attack(creatures[1], attack_summary))
                    attack_results.append(creatures[1].attack(creatures[0]))
                # monster attacks first
                else:
                    attack_results.append(creatures[0].attack(creatures[1]))
                    attack_results.append(creatures[1].attack(creatures[0], attack_summary))
                [c.set_surprised(False) for c in creatures]

            self.state = State.FIGHT

            # perform creature attacks
            # if an attack kills the opposing creature, the fight ends immediately
            for i, attack in enumerate(attack_results):
                j = 1 - i
                kill = creatures[j].damage(attack.damage)
                option = "default"
                if attack.attack_hit == dndclient.ActionOutcome.CRIT_SUCCESS:
                    option = "crit_success"

                # character attack
                if isinstance(creatures[i], Character):
                    if attack.attack_hit.value > 0:
                        attack_summaries.append(lang.attack_character.result.choose(
                            attack.name,
                            creatures[j].name,
                            attack.damage,
                            option=option
                        ))

                    # use up spell slots
                    spells = set()
                    for a in attack.attacks:
                        if isinstance(a, AttackSpell) and a.spell.index not in spells:
                            spells.add(a.spell.index)
                            self.character.spell_slots[a.slot_level].use()

                    if kill:
                        attack_summaries.append(lang.death_monster.result.choose(
                            self.monster.name
                        ))

                        db.create_combat_log(creatures[i], creatures[j], attack.name)
                        creatures[j].died_at = datetime.now(timezone.utc)
                        creatures[i].stats["kills"] += 1

                        inv_check = self.character.skill_check("investigation")
                        per_check = self.character.skill_check("perception")
                        gold_rolls = 1
                        if inv_check == 20 or per_check == 20:
                            gold_rolls = 3
                        elif inv_check == 0 or per_check == 0:
                            gold_rolls = 0
                        elif max(inv_check, per_check) > 10:
                            gold_rolls = 2

                        # earn loot
                        diff_ratio = (dndclient.CHALLENGE_RATINGS.index(self.monster.challenge_rating) + 1) / len(dndclient.CHALLENGE_RATINGS)
                        loot_chance = util.clamp((diff_ratio * 2 * max(inv_check, per_check)) / 100, 0, 1)

                        drops = list(self.monster.drops())
                        if len(drops) > 0 and (inv_check == 20 or random.random() <= loot_chance):
                            item = random.choice(drops)
                            self.character.equip_item(item)
                            if isinstance(item, MagicItem):
                                attack_summaries.append(lang.death_monster.result.loot_effect.choose(
                                    item.name,
                                    self.monster.name,
                                    item.effect_str
                                ))

                        # earn gold and xp
                        reward_gold = max(1, math.ceil(self.monster.challenge_rating * Dice("1d6").roll(gold_rolls)))
                        reward_gold = int(reward_gold * float(util.config["gold_mult"]))
                        creatures[i].earn(Currency(reward_gold, Unit.gp))

                        reward_xp = int(self.monster.xp * float(util.config["exp_mult"]))
                        leveled_up = creatures[i].add_experience(reward_xp)
                        if leveled_up:
                            self.map_append(util.emojis["experience"]["level"])

                        attack_summaries.append("+{}{} +{}{}".format(
                            reward_gold,
                            util.emojis["equipment"]["gold"],
                            reward_xp,
                            util.emojis["experience"]["xp"]
                        ))

                        # the monster died, so we need to make sure its updated before
                        # we discard it
                        db.update_campaign(self)
                        self.state = State.ADVENTURE
                        self.monster = None
                        return "\n".join(attack_summaries)
                # monster attack
                else:
                    if attack.attack_hit.value > 0:
                        attack_summaries.append(lang.attack_monster.result.choose(
                            creatures[i].name,
                            attack.name,
                            attack.damage,
                            option=option
                        ))

                    # use up spell slots
                    spells = set()
                    for a in attack.attacks:
                        if isinstance(a, AttackSpell) and a.spell.index not in spells:
                            spells.add(a.spell.index)
                            self.monster.spell_slots[a.slot_level].use()

                    if kill:
                        # excess damage causes instant kill
                        if abs(creatures[j].hitpoints) >= creatures[j].max_hitpoints:
                            attack_summaries.append(lang.death_character.result.choose(option="instant"))
                            db.create_combat_log(creatures[i], creatures[j], attack.name)
                            self.state = State.END
                            return "\n".join(attack_summaries)

                        elif creatures[j].death_saving_throws():
                            attack_summaries.append(lang.death_character.result.choose(option="unconscious"))

                        else:
                            attack_summaries.append(lang.death_character.result.choose())
                            db.create_combat_log(creatures[i], creatures[j], attack.name)
                            self.state = State.END
                            return "\n".join(attack_summaries)

            return "\n".join(attack_summaries)

        return Option(
            lang.options.attack_option.choose(
                attack_summary.get_prompt()
            ),
            action
        )


    def option_run(self, monster: Monster) -> Option:

        def action() -> str:
            attack_summary = []
            run_result = self.character.run(self.monster)
            monster_attack = None

            match run_result:
                # Can't get away and monster gets an attack
                case dndclient.ActionOutcome.CRIT_FAILURE:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    attack_summary.append(lang.run_character.result.choose(option="crit_failure"))
                    if monster_attack.damage > 0:
                        attack_summary.append(lang.attack_monster.result.choose(
                            self.monster.name,
                            monster_attack.name,
                            monster_attack.damage
                        ))
                    self.state = State.FIGHT

                # May get away, but if monster attack hits, combat resumes
                case dndclient.ActionOutcome.FAILURE:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        self.state = State.FIGHT
                        option = "failure"

                    attack_summary.append(lang.run_character.result.choose(
                        monster.name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))

                    if monster_attack.damage <= 0:
                        self.state = State.ADVENTURE
                        self.monster = None

                # Get away, but monster can attack
                case dndclient.ActionOutcome.SUCCESS:
                    monster_attack = monster.attack(self.character, can_crit=False)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        option = "success"

                    attack_summary.append(lang.run_character.result.choose(
                        monster.name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))
                    self.state = State.ADVENTURE
                    self.monster = None

                # Always get away with no consequences
                case dndclient.ActionOutcome.CRIT_SUCCESS:
                    attack_summary.append(lang.run_character.result.choose(option="crit_success"))
                    self.state = State.ADVENTURE
                    self.monster = None

            if monster_attack and monster_attack.damage > 0:
                excess_damage = monster_attack.damage - self.character.hitpoints
                if monster_attack and self.character.damage(monster_attack.damage):
                    if excess_damage >= self.character.max_hitpoints:
                        db.create_combat_log(monster, self.character, monster_attack.name)
                        attack_summary.append(lang.death_character.result.choose(option="instant"))
                        self.state = State.END

                    elif self.character.death_saving_throws():
                        attack_summary.append(lang.death_character.result.choose(option="unconscious"))

                    else:
                        db.create_combat_log(monster, self.character, monster_attack.name)
                        attack_summary.append(lang.death_character.result.choose())
                        self.state = State.END

            return "\n".join(attack_summary)

        return Option(
            lang.options.run.choose(
                util.emojis['prompts']['run']
            ),
            action
        )


    def option_avoid(self, monster: Monster = None, name: str = "") -> Option:

        def action() -> str:
            nonlocal name
            if monster and not name:
                name = monster.name

            if monster is None:
                self.state = State.ADVENTURE
                return lang.avoid.result.choose(name, option="success")

            elif self.character.avoid(monster).value > 0:
                self.state = State.ADVENTURE
                return lang.avoid.result.choose(name, option="success")

            else:
                mi = monster.roll_initiative()
                ci = self.character.roll_initiative()
                util.debug(f"MI: {mi}, CI: {ci}")
                self.monster = monster
                self.character.recent_rolls["Initiative"] = ci
                self.monster.recent_rolls["Initiative"] = mi
                self.monster.id = db.create_monster(self.monster, self)
                self.map_append(monster.emoji())
                self.state = State.FIGHT
                return lang.avoid.result.choose(name, option="failure")

        return Option(
            lang.options.avoid.choose(
                util.emojis["skills"]["stealth"]
            ),
            action
        )


    def option_leave(self, text = None) -> Option:

        def action():
            match self.state:
                case State.TREASURE:
                    self.state = State.ADVENTURE
                    return lang.treasure.result.leave.choose()

                case State.SHOP:
                    self.state = State.TOWN
                    return lang.shop.result.leave.choose()

                case State.TRADER:
                    self.state = State.ADVENTURE
                    return lang.trader.result.leave.choose()

                case _:
                    raise ValueError(f"Invalid parent state {self.state}")

        if text is None:
            text = lang.options.leave.choose(util.emojis["prompts"]["back"])
        return Option(text, action)

#endregion

    def bio(self) -> str:
        """Get the string to display in the bot bio.
        """
        delta = datetime.now(timezone.utc) - self.start_time
        return "{}\n\n{}\n{} {} {} {} {} {}".format(
            self.character.bio(),
            self.map_str(),
            util.emojis["campaign"]["id"],
            self.id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds()),
            util.emojis["campaign"]["user"],
            self.votes
        )


    def command_handler(self, user: dict, status: dict) -> None:
        """Handle commands sent by mentions.
        """
        result = re.search(f"(^|\s|<br />|<br/>|<br>)+{util.config['command_prefix']}(\w+)( ( |\w)*)?", status["content"])

        if not result:
            return

        command = result.group(2).lower()
        args = []
        if result.group(3):
            args = re.sub(" {2,}", " ", result.group(3).strip().lower()).split(" ")
        util.debug(f"Received command '{command}' with args '{args}'")

        match command:
            # Display proficient armor
            case "armor":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                armors = self.character.proficient_armor()
                armor_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for armor in armors:
                    cost = armor.cost
                    if afford and cost > self.character.currency:
                        continue
                    armor_str.append((cost, f"{armor.name} ({int(cost.amount)})"))

                if not armor_str:
                    armor_str.append((0, "None"))

                armor_str = sorted(armor_str, key=lambda a: a[0], reverse=not asc)
                client.command_reply(", ".join([a[1] for a in armor_str]), status, command)

            # Display author URL
            case "author":
                client.command_reply(util.config["author_url"], status, command)

            # Display the character avatar
            case "avatar":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return
                if not os.path.exists(image.AI_AVATAR_ORIGINAL_PATH):
                    client.command_reply("Character avatar currently unavailable.", status, command)
                    return
                client.command_reply(
                    f"{self.character.title()}'s Avatar",
                    status, command,
                    image_path=image.AI_AVATAR_ORIGINAL_PATH,
                    image_alt="AI-generated Character avatar"
                )

            # Blacklist the given user from being chosen
            case "blacklist":
                if str(user["id"]) not in util.blacklist["b"]:
                    util.blacklist["b"].append(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.blacklisted.choose(), status, command)

            # Give a link to the campaigns tag
            case "campaigns":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_CAMPAIGN_START.removeprefix("#")
                ), status, command)

            # Display character information
            case "character":
                if self.character:
                    client.command_reply(
                        self.character.extended_bio(),
                        status,
                        command,
                        image_path=image.AI_AVATAR_ORIGINAL_PATH,
                        image_alt="AI-generated Character avatar"
                    )
                else:
                    client.command_reply("Character currently unavailable.", status, command)

            case "charactersheet":
                if self.character and self.character.id != 0:
                    client.command_reply(
                        "{}/characters/{}".format(
                            util.config["website_url"],
                            self.character.id
                        ),
                        status,
                        command
                    )
                else:
                    client.command_reply("Character sheet not available.", status, command)

            # List the effects on the character
            case "effects":
                if self.character:
                    string = ""
                    for effect_type in EffectType:
                        effect_strs = []
                        for effect in [e for e in self.character.effects if e.effect_type == effect_type]:
                            effect_strs.append(
                                "{} {} {}s".format(effect.effect_stat.name, effect.mod_str(), effect.duration)
                            )
                        if len(effect_strs) > 0:
                            string += "{}\n{}\n\n".format(effect_type.name.title(), ", ".join(effect_strs))
                    if not string:
                        string = "There are no active effects."
                    client.command_reply(string, status, command)

                else:
                    client.command_reply("Character currently unavailable.", status, command)

            case "graves" | "graveyard":
                client.command_reply("{}/graveyard".format(util.config["website_url"]), status, command)

            case "header" | "banner":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return
                if not os.path.exists(image.AI_HEADER_ORIGINAL_PATH):
                    client.command_reply("Character header currently unavailable.", status, command)
                    return
                client.command_reply(
                    f"{self.character.title()}'s Header",
                    status, command,
                    image_path=image.AI_HEADER_ORIGINAL_PATH,
                    image_alt="AI-generated Character header"
                )

            # Display monster information
            case "monster":
                if self.monster:
                    image_path = self.monster.image_url or None
                    client.command_reply(
                        self.monster.extended_bio(),
                        status,
                        command,
                        image_path=image_path,
                        image_alt="AI-generated Monster avatar"
                    )
                else:
                    client.command_reply("Monster currently unavailable.", status, command)

            # Display possible monsters
            case "monsters":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                monster_cr = dndclient.xp_to_max_cr(self.character.xp)
                monsters = dndclient.get_monsters(dndclient.CHALLENGE_RATINGS[:dndclient.CHALLENGE_RATINGS.index(monster_cr) + 1])
                monster_str = []

                for m in monsters:
                    monster = dndclient.get_monster(m["index"])
                    monster_str.append((monster["challenge_rating"], f"{monster['name']} ({monster['challenge_rating']})"))

                asc = args and args[0].startswith("asc")
                monster_str = sorted(monster_str, key=lambda m: m[0], reverse=not asc)
                client.command_reply(", ".join([m[1] for m in monster_str]), status, command)

            # Display recent poll info
            case "posts" | "polls":
                if not self.root_post_url:
                    client.command_reply("No polls available.", status, command)
                    return

                text = f"Root poll: {self.root_post_url}"
                text += f"\nLast completed poll: {self.completed_post_url or 'None'}"
                text += f"\nCurrent poll: {self.current_post_url}"
                client.command_reply(text, status, command)

            # Display the rolls that ocurred
            case "rolls":
                rolls_str = ""

                if self.character and self.character.get_recent_rolls():
                    rolls_str += f"{self.character.emoji()} Character Rolls\n{self.character.get_recent_rolls()}\n\n"

                if self.monster and self.monster.get_recent_rolls():
                    rolls_str += f"{self.monster.emoji()} Monster Rolls\n{self.monster.get_recent_rolls()}"

                if rolls_str:
                    client.command_reply(rolls_str, status, command)
                else:
                    client.command_reply("Rolls currently unavailable.", status, command)

            # Display all items that could appear in a shop (proficient)
            case "shop" | "proficient" | "equipment":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                weapons = self.character.proficient_weapons()
                armors = self.character.proficient_armor()
                weapons_str = []
                armor_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for weapon in weapons:
                    cost = weapon.cost
                    if afford and cost > self.character.currency:
                        continue
                    weapons_str.append((cost, f"{weapon.name} ({cost})"))

                for armor in armors:
                    cost = armor.cost
                    if afford and cost > self.character.currency:
                        continue
                    armor_str.append((cost, f"{armor.name} ({cost})"))

                if not armor_str:
                    armor_str.append((0, "None"))

                weapons_str = sorted(weapons_str, key=lambda w: w[0], reverse=not asc)
                armor_str = sorted(armor_str, key=lambda a: a[0], reverse=not asc)
                client.command_reply(
                    f"Weapons:\n{', '.join([w[1] for w in weapons_str])}\n\n" +
                    f"Armor:\n{', '.join([a[1] for a in armor_str])}",
                    status,
                    command
                )

            # Display source URL
            case "source":
                client.command_reply(util.config["source_url"], status, command)

            # Display learned spells
            case "spells":
                if self.character:
                    spell_str = ""
                    for i, slot in enumerate(self.character.spells):
                        if not slot: continue
                        spell_str += f"{util.emojis['spell_slots'][str(i)]}\n"
                        spell_str += ", ".join([s.name for s in slot])
                        spell_str += "\n"

                    if spell_str:
                        client.command_reply(spell_str, status, command)
                    else:
                        client.command_reply("No spells available.", status, command)
                else:
                    client.command_reply("Character currently unavailable.", status, command)

            # Display status/api status
            case "status" | "ping":
                now_time = datetime.now(timezone.utc)
                text = ""

                if self.start_time:
                    text += "Campaign started: {} ago".format(
                        util.format_timedelta(now_time - self.start_time)
                    )
                if self.current_post_url:
                    post_time = client.mastodon.status(self.current_post_url.split("/")[-1])["created_at"]
                    text += "\nLast post: {} ago".format(
                        util.format_timedelta(now_time - post_time)
                    )

                text += "\n\nInstance health: {}".format(
                    "Healthy" if client.mastodon.instance_health() else "Unhealthy"
                )
                text += "\nD&D API health: {}".format(
                    "Healthy" if dndclient.api_health() else "Unhealthy"
                )

                status_time: datetime = status["created_at"].astimezone(timezone.utc)
                text += "\n\nResponded in {}".format(
                    util.format_timedelta(now_time - status_time)
                )

                client.command_reply(text, status, command)

            # Display link to #tip
            case "tips":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_TIP.removeprefix("#")
                ), status, command)

            # Display link to #update
            case "updates" | "changelog":
                client.command_reply("{}/@{}/tagged/{}".format(
                    client.mastodon.api_base_url,
                    client.mastodon.me()["username"],
                    client.HASHTAG_UPDATE.removeprefix("#")
                ), status, command)

            # Display proficient weapon
            case "weapons":
                if not self.character:
                    client.command_reply("Character currently unavailable.", status, command)
                    return

                weapons = self.character.proficient_weapons()
                weapons_str = []
                asc = args and any(a.startswith("asc") for a in args)
                afford = args and any(a.startswith("afford") for a in args)

                for weapon in weapons:
                    cost = weapon.cost
                    if afford and cost > self.character.currency:
                        continue
                    weapons_str.append((cost, f"{weapon.name} ({cost})"))

                weapons_str = sorted(weapons_str, key=lambda w: w[0], reverse=not asc)
                client.command_reply(", ".join([w[1] for w in weapons_str]), status, command)

            case "website":
                client.command_reply(util.config["website_url"], status, command)

            # Whitelist the given user (removes from blacklist)
            case "whitelist":
                if str(user["id"]) in util.blacklist["b"]:
                    util.blacklist["b"].remove(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.whitelisted.choose(), status, command)

            # Invalid command
            case _:
                commands = [
                    "armor",
                    "author",
                    "avatar",
                    "campaigns",
                    "character",
                    "charactersheet",
                    "blacklist",
                    "effect",
                    "graves", "graveyard",
                    "header", "banner",
                    "help",
                    "monster",
                    "monsters",
                    "posts", "polls",
                    "rolls",
                    "shop", "proficient", "equipment",
                    "source",
                    "status", "ping",
                    "tips",
                    "updates", "changelog",
                    "weapons",
                    "website",
                    "whitelist",
                ]
                help = "Available commands:\n"
                help += ", ".join(commands)
                help += "\n\nDetailed command list here:\n{}/graveyard".format(
                    util.config["website_url"]
                )
                client.command_reply(help, status, "help")
