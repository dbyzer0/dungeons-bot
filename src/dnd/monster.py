import random
from attacks import AttackMonster, AttackResult, AttackSpell, AttackSummary

from creature import Creature
from dice import Dice
import dnd.dndclient as dndclient
from dnd.spell import Spell
from item import Item
import lang
from dnd.magic_item import PotionOfFlying, PotionOfGiantStrength, PotionOfHealing, PotionOfHeroism, PotionOfInvisibility, PotionOfResistance, PotionOfSpeed
import util
import math


class Monster(Creature):
    """
    Describes a Monster, a hostile Creature.
    """

    def __init__(self, monster: dndclient.DNDResponse) -> None:
        """Create a new `Monster`.

        `monster`: monster dict to create the `Monster` from.
        """
        super().__init__(monster["name"], Dice(monster["hit_points_roll"]).roll())
        self._dnd_dict = monster.reduce()
        self.index = str(monster["index"])
        self.monster_type = str(monster["type"])
        self.hit_points_roll = Dice(str(monster["hit_points_roll"]))

        self.alignment = str(monster["alignment"]).lower()
        self.size = str(monster["size"]).lower()

        self.xp = int(monster["xp"])
        self.challenge_rating = float(monster["challenge_rating"])
        self.image_url = dndclient.get_monster_image_link(monster)

        abilities = dndclient.get_abilities()
        for a in abilities:
            ability = dndclient.get_ability(a["index"])
            self.abilities[a["index"]] = monster[ability["full_name"].lower()]

        for p in monster["proficiencies"]:
            proficiency = self.add_proficiency(p["proficiency"]["index"])
            if proficiency.type == "skills":
                self.skills[proficiency.reference["index"]] = p["value"]

            elif proficiency.type == "saving throws":
                self.saving_throws[proficiency.reference["index"]] = p["value"]

        self.proficiency_bonus = int(monster["proficiency_bonus"])

        self.vulnerabilities.update(monster["damage_vulnerabilities"])
        self.resistances.update(monster["damage_resistances"])
        self.immunities.update(monster["damage_immunities"])

        self.senses = list(monster["senses"])
        self.special_abilities = list(monster["special_abilities"])
        self.actions = list(monster["actions"]) if "actions" in monster else []
        self.legendary_actions = list(monster["legendary_actions"])
        # List of action names that are recharging
        self.recharging: set[str] = set()

        # Speeds
        if "walk" in monster["speed"]:
            self.walk_speed = int(str(monster["speed"]["walk"]).split(" ")[0])
        if "swim" in monster["speed"]:
            self.swim_speed = int(str(monster["speed"]["swim"]).split(" ")[0])
        if "fly" in monster["speed"]:
            self.fly_speed = int(str(monster["speed"]["fly"]).split(" ")[0])
        if "climb" in monster["speed"]:
            self.climb_speed = int(str(monster["speed"]["climb"]).split(" ")[0])
        if "burrow" in monster["speed"]:
            self.burrow_speed = int(str(monster["speed"]["burrow"]).split(" ")[0])

        self.ac = list(monster["armor_class"])

        # init spellcasting
        self.spell_level = 0
        self.spell_school = ""
        self.spell_innate = False
        self.spell_dc = 0
        self.spell_modifier = 0

        for special_ability in monster["special_abilities"]:
            if "spellcasting" not in special_ability: continue
            spellcasting = dict(special_ability["spellcasting"])
            self.spell_ability = str(spellcasting["ability"]["index"])

            self.spell_innate = True
            if "school" in spellcasting:
                self.spell_school = str(spellcasting["school"])
                self.spell_innate = False
                self.spell_level = int(spellcasting["level"])

            if "dc" in spellcasting:
                self.spell_dc = int(spellcasting["dc"])

            if "modifier" in spellcasting:
                self.spell_modifier = int(spellcasting["modifier"])

            self.spell_slots[0] = dndclient.SpellSlot(0, cantrip=True)

            if "slots" in spellcasting:
                for slot in spellcasting["slots"]:
                    slot_num = int(slot)
                    slot_count = int(spellcasting["slots"][slot])
                    self.spell_slots[slot_num] = dndclient.SpellSlot(slot_num, slot_count)

            for spell in spellcasting["spells"]:
                # spell doesn't have index for some reason, so use url instead
                dnd_spell = dndclient.get_spell(spell["url"])
                spell_level = int(spell["level"])
                self.spells[spell_level].add(Spell(dnd_spell))
                if spell_level == 0:
                    self.spells_known["cantrips"] += 1
                else:
                    self.spells_known["spells"] += 1

        # Languages
        language_split = str(monster["languages"]).split(", ")
        for language in language_split:
            if language.strip() != "" and dndclient.get_language(language.lower()).valid():
                self.add_language(language.lower())


    def armor_class(self, armor: dict = None) -> int:
        """Get the Armor Class of the `Monster`.

        `armor`: ignored for `Monster`s.
        """
        ac = 0
        for armor in self.ac:
            if armor["type"] == "armor" or armor["type"] == "dex" or armor["type"] == "natural":
                ac = max(ac, armor["value"])

        return ac


    def emoji(self) -> str:
        """Get the emoji associated with this `Monster`, based on type.
        """
        if self.monster_type in util.emojis["monster_types"]:
            return util.emojis["monster_types"][self.monster_type]

        return ""


    def short_bio(self) -> str:
        """Get the short description, usually displayed before interacting.
        """
        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])

        dice = self.hit_points_roll

        return lang.bio_monster.short.choose(
            f"{self.emoji()} {self.name}",
            f"{util.emojis['health']['full']} {max(1, dice.min())}-{dice.max()}",
            " ".join(status_effects),
            f"{util.emojis['equipment']['challenge_rating']}{util.float_to_frac(self.challenge_rating)}",
            f"{util.emojis['experience']['xp']} {self.xp}"
        )


    def bio(self) -> str:
        """Get the long description, usually displayed while interacting.
        """
        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])

        return lang.bio_monster.long.choose(
            f"{self.emoji()} {self.name}",
            f"{self.hitpoint_bar()}",
            f"{util.emojis['equipment']['armor']} {self.armor_class()}",
            " ".join(status_effects),
            f"{util.emojis['equipment']['challenge_rating']}{util.float_to_frac(self.challenge_rating)}",
            f"{util.emojis['experience']['xp']} {self.xp}"
        )


    def extended_bio(self) -> str:
        """Extended bio to be displayed when queried with commands.
        """
        bio = "{} {}\n".format(
            self.emoji(),
            self.name
        )

        bio += "{} {}/{} ".format(
            util.emojis["health"]["full"],
            self.hitpoints,
            self.max_hitpoints
        )
        bio += "{} {} ".format(
            util.emojis["equipment"]["armor"],
            self.armor_class()
        )

        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])
        bio += " ".join(status_effects)

        bio += f"\n{self.abilities_str()}"

        if self.get_vulnerabilities():
            bio += "\n{} {}".format(
                util.emojis["prompts"]["advantage"],
                ", ".join([v.title() for v in self.get_vulnerabilities()])
            )

        if self.get_resistances():
            bio += "\n{} {}".format(
                util.emojis["prompts"]["disadvantage"],
                ", ".join([r.title() for r in self.get_resistances()])
            )

        if self.get_immunities():
            bio += "\n{} {}".format(
                util.emojis["prompts"]["negate"],
                ", ".join([i.title() for i in self.get_immunities()])
            )

        if self.actions:
            bio += "\n{} {}".format(
                util.emojis["attacks"]["melee_2h"],
                ", ".join([a["name"] for a in self.actions])
            )

        bio += "\n{} {} ".format(
            util.emojis["equipment"]["challenge_rating"],
            util.float_to_frac(self.challenge_rating)
        )
        bio += "\n{} {}".format(
            util.emojis["experience"]["xp"],
            self.xp
        )

        return bio


    def drops(self) -> set[Item]:
        """Get loot drops for this monster.
        """
        loot_pool = set()
        diff_ratio = (dndclient.CHALLENGE_RATINGS.index(self.challenge_rating) + 1) / len(dndclient.CHALLENGE_RATINGS)

        if self.is_flier():
            loot_pool.add(PotionOfFlying.inst())

        all_str_pots = PotionOfGiantStrength.all()

        if self.size in ["large", "huge", "gargantuan"]:
            loot_pool.add(all_str_pots[round((len(all_str_pots) - 1) * diff_ratio)])

        for prof in self.proficiencies:
            if prof.index == "skill-stealth":
                loot_pool.add(PotionOfInvisibility.inst())
            elif prof.index == "skill-athletics":
                loot_pool.add(all_str_pots[round((len(all_str_pots) - 1) * diff_ratio)])

        for action in self.actions:
            if "invisibility" in action["name"]:
                loot_pool.add(PotionOfInvisibility.inst())
                break

        for special_ability in self.special_abilities:
            if "invisibility" in special_ability["name"]:
                loot_pool.add(PotionOfInvisibility.inst())
                break

        good_alignments = ["lawful good", "neutral good", "chaotic good"]
        if any(a in self.alignment.lower() for a in good_alignments):
            all_potions = PotionOfHealing.all()
            loot_pool.add(all_potions[round((len(all_potions) - 1) * diff_ratio)])

        if "lawful good" in self.alignment:
            loot_pool.add(PotionOfHeroism.inst())

        for action in self.actions:
            if "damage" not in action: continue
            for damage in action["damage"]:
                if "damage_type" not in damage: continue
                variant = PotionOfResistance.Variant(damage["damage_type"]["index"])
                loot_pool.add(PotionOfResistance(variant))

        if self.get_walk_speed() >= 60\
                or self.get_fly_speed() >= 80\
                or self.get_swim_speed() >= 60:
            loot_pool.add(PotionOfSpeed.inst())

        return loot_pool


    def modify_damage(self, other: Creature, damage_amount: int, damage_type: str) -> int:
        """Modify the damage of an attack based on a creature's stats.

        `other`: the creature to simulate against.
        `damage_amount`: the gross amount of damage to do.
        `damage_type`: the damage type of the attack.

        Returns the amount of damage to deal.
        """
        if other.resistant_to(damage_type=damage_type):
            damage_amount = math.ceil(damage_amount / 2)
            util.debug(f"{other.name} resistant to {damage_type}")
        if other.vulnerable_to(damage_type=damage_type):
            damage_amount *= 2
            util.debug(f"{other.name} vulnerable to {damage_type}")
        if other.immune_to(damage_type=damage_type):
            damage_amount *= 0
            util.debug(f"{other.name} immune to {damage_type}")

        # Immune attacks will still do 1 damage for the sake of this format
        return max(1, damage_amount)


    def get_attacks(self) -> list[AttackSummary]:
        """Construct `Attack`s for every possible action the Monster can take.

        For the sake of multiattack and optional damage type actions, this function
        will randomly choose instead of returning every permutation.
        """
        attack_summaries: list[AttackSummary] = []

        actions = self.actions
        for action in actions:
            if "damage" not in action\
                    and "options" not in action\
                    and "multiattack_type" not in action:
                continue

            attacks = []

            def add_attacks(actions_list: list[dict]) -> None:
                for subaction in actions_list:
                    for a in self.actions:
                        if a["name"] != subaction["action_name"]: continue
                        action_count = subaction["count"]
                        if isinstance(action_count, str):
                            # Hydra 😑
                            if action_count == "Number of Heads":
                                action_count = 5
                            # Dice rolls
                            elif Dice.is_dice(action_count):
                                action_count = Dice(action_count).roll()
                        attacks.extend([AttackMonster(a)] * int(action_count))

            # multiattacks
            if "multiattack_type" in action:
                # static set of actions to perform
                if action["multiattack_type"] == "actions":
                    add_attacks(action["actions"])
                # variable set of actions to perform
                elif action["multiattack_type"] == "action_options":
                    amount = action["action_options"]["choose"]
                    options = action["action_options"]["from"]["options"]
                    for _ in range(amount):
                        choice = random.choice(options)
                        if choice["option_type"] == "multiple":
                            add_attacks(choice["items"])
                        else:
                            add_attacks([choice])

            # variable attack
            elif "options" in action:
                amount = action["options"]["choose"]
                options = action["options"]["from"]["options"]
                for _ in range(amount):
                    attacks.append(AttackMonster(random.choice(options)))

            # normal attacks
            else:
                attacks.append(AttackMonster(action))

            attack_summaries.append(AttackSummary(attacks, action["name"]))

        # spells
        for slot, spell_level in enumerate(self.spells):
            if not self.spell_slots[slot].can_use(): continue
            for spell in spell_level:
                if not spell.damage\
                        or "damage_type" not in spell.damage\
                        or spell.ritual:
                    continue
                attack = AttackSpell(spell, self.spell_modifier, 0, slot, self.spell_level, self.spell_dc)
                attack_summaries.append(AttackSummary([attack], spell.name))

        return attack_summaries


    def attack(self, other: Creature, attack_summary: AttackSummary = None, can_crit = True) -> AttackResult:
        """Simulate an attack against another Creature. Non-damaging actions are ignored where possible,
        doing 1 damage when not.

        `other`: `Creature` to attack.
        `action`: action dict to use for the attack.
        `can_crit`: whether the Monster can crit on the attack or not (used for running)
        """
        if self.get_surprised():
            return AttackResult([], 0, dndclient.ActionOutcome.NONE, "")

        all_attacks = self.get_attacks()

        # monster has no actions or spells, so they do nothing
        if len(all_attacks) == 0 and attack_summary is None:
            return AttackResult([], 0, dndclient.ActionOutcome.NONE, "")

        if attack_summary is None:
            attack_summary = random.choice(all_attacks)
        attack_name = attack_summary.prompt

        attack_hit = dndclient.ActionOutcome.CRIT_FAILURE.value
        total_damage = 0
        attacks_hit = []
        self.recent_rolls["Attack Roll"] = []
        self.recent_rolls["Damage Roll"] = []
        self.recent_rolls["Other Saving Throw"] = []

        util.debug(f"Monster attacking with {attack_summary.prompt}")

        for attack in attack_summary.get_attacks():
            hit_check = 0
            damage_mod = 1

            if not isinstance(attack, AttackMonster | AttackSpell):
                raise TypeError(f"Invalid monster attack type: {attack}")

            # Saving throw attack
            # spells with dc and monster actions with dc
            if attack.dc_type:
                other_save = other.saving_throw(attack.dc_type)
                self.recent_rolls["Other Saving Throw"].append(other_save)
                util.debug(f"Monster DC Roll: {other_save}/{attack.dc_value}")

                # get mod and hit value
                if attack.dc_value >= other_save:
                    damage_mod = 1
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                    attacks_hit.append(attack)
                else:
                    damage_mod = attack.dc_modifier
                    if damage_mod > 0:
                        attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                        attacks_hit.append(attack)
                    else:
                        attack_hit = max(attack_hit, dndclient.ActionOutcome.FAILURE.value)

                # calc damage
                for d in range(len(attack.damage_dice)):
                    damage = attack.damage_dice[d].roll(custom_mod=attack.damage_modifier)
                    self.recent_rolls["Damage Roll"].append(damage)
                    total_damage += math.ceil(self.modify_damage(other, damage, attack.damage_types[d]) * damage_mod)

            # always hit spells (i.e. magic missile)
            elif isinstance(attack, AttackSpell) and not attack.is_attack:
                for d in range(len(attack.damage_dice)):
                    damage = attack.damage_dice[d].roll(custom_mod=attack.damage_modifier)
                    self.recent_rolls["Damage Roll"].append(damage)
                    total_damage += self.modify_damage(other, damage, attack.damage_types[d])
                attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                attacks_hit.append(attack)

            # regular attacks
            else:
                hit_check = Dice("1d20").roll()
                self.recent_rolls["Attack Roll"].append(hit_check)
                util.debug(f"Monster attack roll: {hit_check} + {attack.attack_modifier}")

                # Critical success
                if hit_check == 20 and can_crit:
                    for d in range(len(attack.damage_dice)):
                        roll = attack.damage_dice[d].roll(2)
                        self.recent_rolls["Damage Roll"].append(roll)
                        total_damage += self.modify_damage(other, roll, attack.damage_types[d])
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.CRIT_SUCCESS.value)
                    attacks_hit.append(attack)

                # Critical failure
                elif hit_check == 1:
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.CRIT_FAILURE.value)

                # Normal attack
                elif hit_check + attack.attack_modifier >= other.armor_class():
                    for d in range(len(attack.damage_dice)):
                        roll = attack.damage_dice[d].roll()
                        self.recent_rolls["Damage Roll"].append(roll)
                        total_damage += self.modify_damage(other, roll, attack.damage_types[d])
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.SUCCESS.value)
                    attacks_hit.append(attack)

                # Miss
                else:
                    attack_hit = max(attack_hit, dndclient.ActionOutcome.FAILURE.value)

        return AttackResult(attacks_hit, total_damage, dndclient.ActionOutcome(attack_hit), attack_name)
