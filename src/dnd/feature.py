import dnd.dndclient as dndclient


class Feature:
    """D&D Feature.
    """
    def __init__(self, feature_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = feature_dict.reduce()
        self.index = str(feature_dict["index"])
        self.name = str(feature_dict["name"])
        self.level = int(feature_dict["level"])
        self.prerequisites = list(feature_dict["prerequisites"])
        self.desc = "\n".join(feature_dict["desc"])
        self.clazz = str(feature_dict["class"]["index"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)
