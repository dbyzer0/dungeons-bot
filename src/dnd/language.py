import dnd.dndclient as dndclient


class Language:
    """D&D Language.
    """
    def __init__(self, language_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = language_dict.reduce()
        self.index = str(language_dict["index"])
        self.name = str(language_dict["name"])
        self.desc = ""
        if "desc" in language_dict:
            self.desc = str(language_dict["desc"])
        self.type = str(language_dict["type"]).lower()
        self.script = ""
        if "script" in language_dict:
            self.script = str(language_dict["script"]).lower()
        self.typical_speakers: list[str] = list(language_dict["typical_speakers"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)
