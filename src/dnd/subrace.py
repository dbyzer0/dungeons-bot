import dnd.dndclient as dndclient


class Subrace:
    """D&D Subrace."""
    def __init__(self, subrace_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = subrace_dict.reduce()
        self.index = str(subrace_dict["index"])
        self.name = str(subrace_dict["name"])
        self.desc = str(subrace_dict["desc"])
        self.race = dict(subrace_dict["race"])
        self.ability_bonuses: list[dict] = list(subrace_dict["ability_bonuses"])
        self.starting_proficiencies: list[dict] = list(subrace_dict["starting_proficiencies"])
        self.languages: list[dict] = list(subrace_dict["languages"])
        self.language_options = {}
        if "language_options" in subrace_dict:
            self.language_options = dict(subrace_dict["language_options"])
        self.racial_traits: list[dict] = list(subrace_dict["racial_traits"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)
