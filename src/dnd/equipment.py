import dnd.dndclient as dndclient
from item import Item
from currency import Currency, Unit
from dice import Dice
import util

# Weapons that do not have disadvantage when used underwater
AQUATIC_WEAPON = [
    "dagger",
    "javelin",
    "shortsword",
    "spear",
    "trident",
    "crossbow",
    "net",
    "dart",
]

UNARMED_EQUIPMENT = {
    "index": "unarmed-strike",
    "name": "Unarmed Strike",
    "equipment_category": {
        "index": "unarmed"
    },
    "cost": {
        "quantity": 0,
        "unit": "cp"
    },
    "properties": [],
    "weight": 0,
    "damage": {
        "damage_dice": "1",
        "damage_type": {
            "index": "bludgeoning"
        }
    },
    "weapon_category": "Simple",
    "weapon_range": "Melee",
    "range": {
        "normal": 5,
    },
    "dnd_api_url": "https://www.dnd5eapi.co/api",
    "url": "/api",
}

# TODO: it is better to split Weapons and Armor into their own classes
# but they presently use a lot of similar functions anyway
class Equipment(Item):
    """Represents an equipment item with some utility functions.
    """

    def __init__(self, equipment_dict: dndclient.DNDResponse) -> None:
        """Create a new `Equipment` from the given equipment dict.
        """
        if not Equipment.is_valid(equipment_dict):
            util.warn(f"Created invalid Equipment object from {equipment_dict}")
            return

        super().__init__(equipment_dict["index"], equipment_dict["name"])
        self._dnd_dict = equipment_dict.reduce()

        self.equipment_category = str(equipment_dict["equipment_category"]["index"])
        # TODO: maybe store the real cost, and round elsewhere
        # consumers of Equipment would need to be modified to round themselves
        self.cost = Currency.from_dict(equipment_dict["cost"]).to(Unit.gp, True)
        self.properties = [str(p["index"]) for p in equipment_dict["properties"]]
        self.weight = int(equipment_dict["weight"])

        # weapon-specific
        self.damage_dice = Dice(0)
        self.damage_type = ""
        self.two_handed_damage_dice = Dice(0)
        self.two_handed_damage_type = ""
        self.range = 0
        self.weapon_range = ""
        self.weapon_category = ""
        # if self.is_weapon() or self.equipment_category == "unarmed":
        if "damage" in equipment_dict:
            self.damage_dice = Dice(equipment_dict["damage"]["damage_dice"])
            self.damage_type = str(equipment_dict["damage"]["damage_type"]["index"])
            self.range = int(equipment_dict["range"]["normal"])
            self.weapon_range = str(equipment_dict["weapon_range"]).lower()
            self.weapon_category = str(equipment_dict["weapon_category"]).lower()
            if self.has_property("versatile"):
                self.two_handed_damage_dice = Dice(equipment_dict["two_handed_damage"]["damage_dice"])
                self.two_handed_damage_type = str(equipment_dict["two_handed_damage"]["damage_type"]["index"])

        self.silvered = False
        self.magical = False
        self.adamantine = False

        # armor-specific
        self.armor_category = ""
        self.armor_class = 0
        self.armor_class_dex = False
        self.armor_class_max_dex = 0
        self.str_minimum = 0
        self.stealth_disadvantage = False
        if self.is_armor():
            self.armor_category = str(equipment_dict["armor_category"]).lower()
            self.armor_class = int(equipment_dict["armor_class"]["base"])
            self.armor_class_dex = bool(equipment_dict["armor_class"]["dex_bonus"])
            self.str_minimum = int(equipment_dict["str_minimum"])
            self.stealth_disadvantage = bool(equipment_dict["stealth_disadvantage"])
            if "max_bonus" in equipment_dict["armor_class"]:
                self.armor_class_max_dex = int(equipment_dict["armor_class"]["max_bonus"])

        self.emoji = self.get_emoji()

        if (self.is_weapon() or self.is_shield())\
                and (not self.has_property("two-handed") and not self.has_property("one-handed")):
            self.properties.append("one-handed")


    @staticmethod
    def is_valid(equipment_dict: dndclient.DNDResponse) -> bool:
        """Determine if the Equipment is valid or not.

        Sometimes we may try to construct Equipment from invalid objects, this
        function lets us verify the Equipment is complete.
        """
        return "index" in equipment_dict


    def is_armor(self) -> bool:
        """Determine if the equipment is armor.
        """
        return self.equipment_category == "armor"


    def is_shield(self) -> bool:
        """Determine if the equipment is a shield (armor that is held in a hand).
        """
        return self.armor_category == "shield"


    def is_weapon(self) -> bool:
        """Determine if the equipment is a weapon.
        """
        return self.equipment_category == "weapon" and self.damage_type


    def get_emoji(self) -> str:
        """Get the emoji associated with this equipment.
        """
        if self.is_armor():
            return util.emojis["equipment"]["armor"]

        return util.emojis["equipment"]["weapon_2h" if self.has_property("two-handed") else "weapon_1h"]


    def get_property_emojis(self) -> list[str]:
        """Get the emojis for each property of the equipment.
        """
        emojis = []
        for prop in self.properties:
            if prop in util.emojis["weapon_properties"]:
                emojis.append(util.emojis["weapon_properties"][prop])

        # "custom" properties
        if self.silvered:
            emojis.append(util.emojis["weapon_properties"]["silvered"])

        return emojis


    def has_property(self, property_index: str) -> bool:
        """Determine if the equipment has the given property. Usually only applies
        to weapons.

        `property_index`: index of the property to find.
        """
        return property_index in self.properties


    def set_silvered(self, is_silvered: bool) -> None:
        """Set the silvered status of the weapon. A silvered weapon increases
        in value by 50 GP.

        `is_silvered`: whether the weapon should be silvered or not.
        """
        if is_silvered:
            self.cost = self.cost + Currency(50)
        else:
            self.cost = self.cost - Currency(50)
        self.silvered = is_silvered


    def is_aquatic_weapon(self) -> bool:
        """Determine if the weapon can be used in water without disadvantage.
        """
        return self.is_weapon()\
            and any(a in self.index for a in AQUATIC_WEAPON)


    def get_weapon_ability(self) -> list[str]:
        """Get the abilities associated with the weapon.
        """
        if not self.is_weapon():
            raise ValueError(f"Equipment '{self.name}' is not a weapon.")

        if self.has_property("finesse"):
            return ["str", "dex"]

        if self.weapon_range == "melee":
            return ["str"]

        if self.weapon_range == "ranged":
            return ["dex"]

        # If no matches above:
        raise ValueError(f"Weapon ability could not be determined! ({self})")


    def compatible_with(self, other: "Equipment") -> bool:
        """Get whether this Equipment is compatible (can be equipped alongside)
        another Equipment.

        `other`: the other Equipment to check compatibility with.
        """
        if self.is_armor():
            return False

        # 2 light weapons,
        # or 1 one-handed weapon and 1 shield,
        # or 1 shield and 1 one-handed weapon,
        # or 2 shields
        return (self.has_property("light") and other.has_property("light"))\
            or (self.has_property("one-handed") and other.is_shield())\
            or (self.is_shield() and other.has_property("one-handed"))\
            or (self.is_shield() and other.is_shield())


    def nice_name(self) -> str:

        if ", " not in self.name:
            return self.name
        name_split = self.name.split(", ")
        return f"{name_split[1].title()} {name_split[0].title()}"


    @staticmethod
    def net_spend(old_equipment: list["Equipment"], new_equipment: list["Equipment"], buy_scale = 1.0, sell_scale = 0.5) -> Currency:
        """Determine net spend when equipping new equipment. Takes the cost of new
        equipment plus the gain by selling existing equipment.

        `old_equipment`: equipment being sold.
        `new_equipment`: equipment being bought.
        `buy_scale`: amount to scale buying prices by, default 1.0 (full price).
        `sell_scale`: amount to scale selling prices by, default 0.5 (half price).
        """
        sell = 0
        if old_equipment:
            sell = sum([e.cost.amount for e in old_equipment])
            sell = sell * sell_scale
        buy = sum([e.cost.amount for e in new_equipment])
        buy = buy * buy_scale
        return Currency(buy - sell, Unit.gp)


    def __str__(self) -> str:

        return str(self._dnd_dict)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(__value, Equipment):
            return False

        return self._dnd_dict == __value._dnd_dict


    # def __getitem__(self, item):

    #     return self._dict[item]
