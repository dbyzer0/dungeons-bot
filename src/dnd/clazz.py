import dnd.dndclient as dndclient


class Class:
    """D&D Class
    """
    def __init__(self, class_dict: dndclient.DNDResponse) -> None:

        self._dnd_dict = class_dict.reduce()
        self.index = str(class_dict["index"])
        self.name = str(class_dict["name"])
        self.hit_die = int(class_dict["hit_die"])
        self.class_levels = str(class_dict["class_levels"])
        self.multi_classing = dict(class_dict["multi_classing"])
        self.spellcasting = {}
        if "spellcasting" in class_dict:
            self.spellcasting = dict(class_dict["spellcasting"])
        self.spells = ""
        if "spells" in class_dict:
            self.spells = str(class_dict["spells"])
        self.starting_equipment: list[dict] = list(class_dict["starting_equipment"])
        self.starting_equipment_options: list[dict] = list(class_dict["starting_equipment_options"])
        self.proficiency_choices: list[dict] = list(class_dict["proficiency_choices"])
        self.proficiencies: list[dict] = list(class_dict["proficiencies"])
        self.saving_throws: list[dict] = list(class_dict["saving_throws"])
        self.subclasses: list[dict] = list(class_dict["subclasses"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)
