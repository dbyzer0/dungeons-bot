from datetime import timedelta
from enums import Rarity
from item import Item
from enum import Enum
from dice import Dice
import dnd.dndclient as dndclient
import lang
import util
from currency import Currency, Unit

from enum import Enum
from collections import namedtuple

RARITY_WEIGHTS = {
    Rarity.VARIES: 3,
    Rarity.COMMON: 5,
    Rarity.UNCOMMON: 4,
    Rarity.RARE: 3,
    Rarity.VERY_RARE: 2,
    Rarity.LEGENDARY: 1
}

RARITY_COSTS = {
    Rarity.VARIES: 1,
    Rarity.COMMON: 50,
    Rarity.UNCOMMON: 100,
    Rarity.RARE: 500,
    Rarity.VERY_RARE: 5000,
    Rarity.LEGENDARY: 50000
}


class MagicItem(Item):
    """A Magic item.
    """
    def __init__(self, magic_item_dict: dndclient.DNDResponse) -> None:
        """Create a new magic item from the given dict.
        """
        super().__init__(magic_item_dict["index"], magic_item_dict["name"], util.emojis["equipment"]["gem"])
        self._dnd_dict = magic_item_dict.reduce()

        self.equipment_category = str(magic_item_dict["equipment_category"]["index"])
        self.rarity = Rarity(str(magic_item_dict["rarity"]["name"]).lower().replace(" ", "_"))
        self.variant = bool(magic_item_dict["variant"])
        self.variants = [MagicItem(dndclient.get_magic_item(v["index"])) for v in magic_item_dict["variants"]]
        self.cost = Currency(RARITY_COSTS[self.rarity], Unit.gp)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(__value, MagicItem):
            return False

        return self._dnd_dict == __value._dnd_dict and hash(self) == hash(__value)


    def __hash__(self) -> int:

        return hash(f"{self.index}-{self.name}")


    def __str__(self) -> str:

        return f"{self.name} ({self.rarity.value.title()})"


class PotionOfHealing(MagicItem):
    """Potion of Healing: https://www.dnd5eapi.co/api/magic-items/potion-of-healing
    """

    class Variant(Enum):

        COMMON = "common"
        GREATER = "greater"
        SUPERIOR = "superior"
        SUPREME = "supreme"


    _HEALING = {
        Variant.COMMON: Dice("2d4+2"),
        Variant.GREATER: Dice("4d4+4"),
        Variant.SUPERIOR: Dice("8d4+8"),
        Variant.SUPREME: Dice("10d4+20")
    }

    def __init__(self, variant: Variant) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-healing-{variant.value}"))
        self.healing = PotionOfHealing._HEALING[variant]
        self.empty = False
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.healing


    @staticmethod
    def common() -> "PotionOfHealing":

        return PotionOfHealing(PotionOfHealing.Variant.COMMON)


    @staticmethod
    def greater() -> "PotionOfHealing":

        return PotionOfHealing(PotionOfHealing.Variant.GREATER)


    @staticmethod
    def superior() -> "PotionOfHealing":

        return PotionOfHealing(PotionOfHealing.Variant.SUPERIOR)


    @staticmethod
    def supreme() -> "PotionOfHealing":

        return PotionOfHealing(PotionOfHealing.Variant.SUPREME)


    @staticmethod
    def all() -> list["PotionOfHealing"]:

        return [
            PotionOfHealing.common(),
            PotionOfHealing.greater(),
            PotionOfHealing.superior(),
            PotionOfHealing.supreme()
        ]


    def use(self) -> int:
        """Use the healing potion, returning the amount to heal. A potion can
        only be used once.
        """
        if self.empty:
            return 0

        healing = self.healing.roll()

        self.empty = True
        self.effect_str = self.effect_str.choose(
            healing
        )

        return healing


class PotionOfGiantStrength(MagicItem):
    """Potion of Giant Strength: https://www.dnd5eapi.co/api/magic-items/potion-of-giant-strength
    """

    class Variant(Enum):

        HILL = "hill"
        FROST = "frost"
        STONE = "stone"
        FIRE = "fire"
        CLOUD = "cloud"
        STORM = "storm"


    _STRENGTH = {
        Variant.HILL: 21,
        Variant.FROST: 23,
        Variant.STONE: 23,
        Variant.FIRE: 25,
        Variant.CLOUD: 27,
        Variant.STORM: 29
    }

    def __init__(self, variant: Variant) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-giant-strength-{variant.value}"))
        self.strength = PotionOfGiantStrength._STRENGTH[variant]
        self.empty = False
        self.duration = 3600
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.strength_increased


    @staticmethod
    def hill() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.HILL)


    @staticmethod
    def frost() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.FROST)


    @staticmethod
    def stone() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.STONE)


    @staticmethod
    def fire() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.FIRE)


    @staticmethod
    def cloud() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.CLOUD)


    @staticmethod
    def storm() -> "PotionOfGiantStrength":

        return PotionOfGiantStrength(PotionOfGiantStrength.Variant.STORM)


    @staticmethod
    def all() -> list["PotionOfGiantStrength"]:

        return [
            PotionOfGiantStrength.hill(),
            PotionOfGiantStrength.frost(),
            PotionOfGiantStrength.stone(),
            PotionOfGiantStrength.fire(),
            PotionOfGiantStrength.cloud(),
            PotionOfGiantStrength.storm()
        ]


    def use(self) -> int:
        """Use the strength potion, returning the amount to set strength to. A potion can
        only be used once.
        """
        if self.empty:
            return 0

        self.empty = True
        self.effect_str = self.effect_str.choose(
            self.strength,
            util.format_timedelta(timedelta(seconds=self.duration))
        )

        return self.strength


class PotionOfFlying(MagicItem):
    """Potion of Flying: https://www.dnd5eapi.co/api/magic-items/potion-of-flying
    """
    def __init__(self) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-flying"))
        self.empty = False
        self.walk_speed_ratio = 1.0
        self.duration = 3600
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.flying


    @staticmethod
    def inst() -> "PotionOfFlying":

        return PotionOfFlying()


    @staticmethod
    def all() -> list["PotionOfFlying"]:

        return [
            PotionOfFlying.inst()
        ]


    def use(self) -> int:
        """Use the flying potion, returning ratio of walking speed to fly. A potion can
        only be used once.
        """
        if self.empty:
            return 0

        self.empty = True
        self.effect_str = self.effect_str.choose(
            util.format_timedelta(timedelta(seconds=self.duration))
        )

        return self.walk_speed_ratio


class PotionOfHeroism(MagicItem):
    """Potion of Heroism: https://www.dnd5eapi.co/api/magic-items/potion-of-heroism
    """
    def __init__(self) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-heroism"))
        self.empty = False
        self.temp_hitpoints = 10
        self.bless_roll = Dice("1d4")
        self.duration = 3600
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.heroism


    @staticmethod
    def inst() -> "PotionOfHeroism":

        return PotionOfHeroism()


    @staticmethod
    def all() -> list["PotionOfHeroism"]:

        return [
            PotionOfHeroism.inst()
        ]


    def use(self):
        """Use the heroism potion, returning the temp hit points to gain. A potion can
        only be used once.
        """
        Heroism = namedtuple("Heroism", ["temp_hitpoints", "bless"])
        if self.empty:
            return Heroism(0, 0)

        bless_roll = self.bless_roll.roll()

        self.empty = True
        self.effect_str = self.effect_str.choose(
            self.temp_hitpoints,
            bless_roll,
            util.format_timedelta(timedelta(seconds=self.duration))
        )

        return Heroism(self.temp_hitpoints, bless_roll)


class PotionOfInvisibility(MagicItem):
    """Potion of Invisibility: https://www.dnd5eapi.co/api/magic-items/potion-of-invisibility
    """
    def __init__(self) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-invisibility"))
        self.empty = False
        self.duration = 3600
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.invisibility


    @staticmethod
    def inst() -> "PotionOfInvisibility":

        return PotionOfInvisibility()


    @staticmethod
    def all() -> list["PotionOfInvisibility"]:

        return [
            PotionOfInvisibility.inst()
        ]


    def use(self) -> bool:
        """Use the invisibility potion, returning True if used successfully. A potion can
        only be used once.
        """
        if self.empty:
            return False

        self.empty = True
        self.effect_str = self.effect_str.choose(
            util.format_timedelta(timedelta(seconds=self.duration))
        )

        return True


class PotionOfResistance(MagicItem):
    """Potion of Resistance: https://www.dnd5eapi.co/api/magic-items/potion-of-resistance
    """
    class Variant(Enum):

        ACID = "acid"
        BLUDGEONING = "bludgeoning" # custom type
        COLD = "cold"
        FIRE = "fire"
        FORCE = "force"
        LIGHTNING = "lightning"
        NECROTIC = "necrotic"
        PIERCING = "piercing" # custom type
        POISON = "poison"
        PSYCHIC = "psychic"
        RADIANT = "radiant"
        SLASHING = "slashing" # custom type
        THUNDER = "thunder"


    def __init__(self, variant: Variant) -> None:

        if variant in [
            PotionOfResistance.Variant.BLUDGEONING,
            PotionOfResistance.Variant.PIERCING,
            PotionOfResistance.Variant.SLASHING
        ]:
            super().__init__(dndclient.get_magic_item(f"potion-of-resistance-{PotionOfResistance.Variant.FORCE.value}"))
            self.name = f"Potion of {variant.value.title()} Resistance"
        else:
            super().__init__(dndclient.get_magic_item(f"potion-of-resistance-{variant.value}"))
        self.empty = False
        self.duration = 3600
        self.variant_type = variant
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.resistance


    @staticmethod
    def acid() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.ACID)


    @staticmethod
    def bludgeoning() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.BLUDGEONING)


    @staticmethod
    def cold() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.COLD)


    @staticmethod
    def fire() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.FIRE)


    @staticmethod
    def force() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.FORCE)


    @staticmethod
    def lightning() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.LIGHTNING)


    @staticmethod
    def necrotic() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.NECROTIC)


    @staticmethod
    def piercing() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.PIERCING)


    @staticmethod
    def poison() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.POISON)


    @staticmethod
    def psychic() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.PSYCHIC)


    @staticmethod
    def radiant() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.RADIANT)


    @staticmethod
    def slashing() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.SLASHING)


    @staticmethod
    def thunder() -> "PotionOfResistance":

        return PotionOfResistance(PotionOfResistance.Variant.THUNDER)


    @staticmethod
    def all() -> list["PotionOfResistance"]:

        return [
            PotionOfResistance.acid(),
            PotionOfResistance.bludgeoning(),
            PotionOfResistance.cold(),
            PotionOfResistance.fire(),
            PotionOfResistance.force(),
            PotionOfResistance.lightning(),
            PotionOfResistance.necrotic(),
            PotionOfResistance.piercing(),
            PotionOfResistance.poison(),
            PotionOfResistance.psychic(),
            PotionOfResistance.radiant(),
            PotionOfResistance.slashing(),
            PotionOfResistance.thunder()
        ]


    def use(self) -> Variant | None:
        """Use the resistance potion, returning the resistance type. A potion can
        only be used once.
        """
        if self.empty:
            return None

        self.empty = True
        self.effect_str = self.effect_str.choose(
            self.variant_type.value,
            util.format_timedelta(timedelta(seconds=self.duration))
        )
        return self.variant_type


class PotionOfSpeed(MagicItem):
    """Potion of Speed: https://www.dnd5eapi.co/api/magic-items/potion-of-speed
    """
    def __init__(self) -> None:

        super().__init__(dndclient.get_magic_item(f"potion-of-speed"))
        self.empty = False
        self.duration = 3600
        self.emoji = util.emojis["equipment"]["potion"]
        self.effect_str = lang.effects_character.haste


    @staticmethod
    def inst() -> "PotionOfSpeed":

        return PotionOfSpeed()


    @staticmethod
    def all() -> list["PotionOfSpeed"]:

        return [
            PotionOfSpeed.inst()
        ]


    def use(self) -> bool:
        """Use the invisibility potion, returning True if used successfully. A potion can
        only be used once.
        """
        if self.empty:
            return False

        self.empty = True
        self.effect_str = self.effect_str.choose(
            util.format_timedelta(timedelta(seconds=self.duration))
        )

        return True
