from copy import deepcopy
from datetime import datetime, timezone
from io import BytesIO
import logging
from mastodon import Mastodon, StreamListener, MastodonError
import os
import random
import string
from time import sleep, monotonic
import multiprocessing

import requests
from tenacity import before_sleep_log, retry, retry_if_exception_type, stop_after_attempt, wait_fixed
import db
import image

import util

# Maximum number of options in a poll on this instance
MAX_POLL_SIZE = 4
# Maximum length of a status on this instance
STATUS_LIMIT = 500
# Maximum length of a single poll option on this instance
POLL_OPTION_LIMIT = 50
# Minimum poll duration on this instance
MIN_POLL_DURATION = 300
# Maximum poll duration on this instance
MAX_POLL_DURATION = 2629746

# TODO: define hashtags in the config file instead
# Hashtag to use for tips
HASHTAG_TIP = "#tip"
# Hashtag to use for campaign start statuses
HASHTAG_CAMPAIGN_START = "#dnd"
# Hashtag to use for campaign end statuses
HASHTAG_CAMPAIGN_END = "#dndend"
# Hashtag to use for character level ups
HASHTAG_CHARACTER_LEVELUP = "#levelup"
# Hashtag to use for update posts
HASHTAG_UPDATE = "#update"

# Content Warning for tips
CW_TIP = "Tip"

# Whether to actually make posts or not
dry = False
# When dry, used for simulating poll ids
poll_id = 0
# When dry, used for keeping track of past poll options
last_poll: list[str] = []
# The last status ID that was posted
status_chain: int = None
# Mastodon client
mastodon: Mastodon = None
# Command listener
listener: "CommandListener" = None
# Stream handler for commands
command_handler = None
# Thread to post tips on
tip_thread: multiprocessing.Process = None
# Thread to generate avatar images on
avatar_image_thread: multiprocessing.Process = None
# Thread to generate header images on
header_image_thread: multiprocessing.Process = None


def init_client():
    """Initialize the Mastodon connection.
    """
    global mastodon, listener, command_handler
    global STATUS_LIMIT, MAX_POLL_SIZE, POLL_OPTION_LIMIT, MIN_POLL_DURATION, MAX_POLL_DURATION

    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    configuration = mastodon.instance()["configuration"]
    STATUS_LIMIT = configuration["statuses"]["max_characters"]
    MAX_POLL_SIZE = configuration["polls"]["max_options"]
    POLL_OPTION_LIMIT = configuration["polls"]["max_characters_per_option"]
    MIN_POLL_DURATION = configuration["polls"]["min_expiration"]
    MAX_POLL_DURATION = configuration["polls"]["max_expiration"]

    listener = CommandListener()
    command_handler = mastodon.stream_user(listener, run_async=True, reconnect_async=True, reconnect_async_wait_sec=10)


def close():
    """Close connections.
    """
    global command_handler
    if command_handler and not command_handler.closed:
        command_handler.close()


def start_image_gen(character):
    """Start a process to generate a new profile photo.

    `character`: the `Character` to generate from.
    """
    if dry:
        return
    global avatar_image_thread
    avatar_image_thread = multiprocessing.Process(target=_image_gen_avatar, args=(character,), daemon=True)
    avatar_image_thread.start()
    global header_image_thread
    header_image_thread = multiprocessing.Process(target=_image_gen_header, args=(character,), daemon=True)
    header_image_thread.start()


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def _image_gen_avatar(character):
    """Generates a new photo photo and sets it.

    `character`: the `Character` to generate from.
    """
    urls = image.generate_avatar(character)

    if urls is None:
        reset_avatar()
        return

    db.upload_file(urls[0], f"{character.id}-avatar.png")
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    mastodon.account_update_credentials(avatar=urls[1])
    util.debug("Updated account avatar")


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def _image_gen_header(character):
    """Generates a new photo photo and sets it.

    `character`: the `Character` to generate from.
    """
    urls = image.generate_header(character)

    if urls is None:
        reset_header()
        return

    db.upload_file(urls[0], f"{character.id}-header.png")
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    mastodon.account_update_credentials(header=urls[1])
    util.debug("Updated account header")


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def reset_avatar():
    """Reset the avatar to the default, if available.
    """
    if dry:
        return
    default_avatar = image.DEFAULT_AVATAR_PATH
    if os.path.exists(default_avatar):
        mastodon.account_update_credentials(avatar=default_avatar)


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def reset_header():
    """Reset the header to the default, if available.
    """
    if dry:
        return
    default_header = image.DEFAULT_HEADER_PATH
    if os.path.exists(default_header):
        mastodon.account_update_credentials(header=default_header)


def start_tips():
    """Start a thread to display tips on an interval.
    """
    global tip_thread
    tip_thread = multiprocessing.Process(target=_tips, daemon=True)
    tip_thread.start()


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def _tips():
    """Posts tips on an interval.
    """
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    interval = util.config["tip_interval"]
    next_time = monotonic() + interval

    while mastodon:
        sleep(max(0, next_time - monotonic()))
        util.info("Posting tip")
        tip = random.choice(list(util.tips.values()))
        tip += f"\n{HASHTAG_TIP}"
        size = len(tip) + len(CW_TIP)
        if size > STATUS_LIMIT:
            tip = tip[:STATUS_LIMIT - len(CW_TIP) - 3] + "..."
            util.warn("Tried to post a tip exceeding the character limit.")
        if not dry:
            mastodon.status_post(tip, spoiler_text=CW_TIP)
        # Skips execution if posting took longer than interval
        next_time += ((monotonic() - next_time) // (interval * interval)) + interval


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_media_post(image_path: str, alt_text: str = "") -> dict | None:
    """Generate a media post dict from the given image.

    `image_path`: path or URL to the image.
    `alt_text`: alt text to use.

    Returns the media post dict, or None if an invalid image.
    """
    media_post = None
    if dry:
        return media_post

    try:
        extension = os.path.splitext(image_path)[1].replace(".", "")
        if os.path.exists(image_path):
            media_post = mastodon.media_post(
                image_path,
                description=alt_text,
                file_name=f"image.{extension}",
            )
        else:
            image_req = requests.get(image_path)
            image = BytesIO(image_req.content)
            media_post = mastodon.media_post(
                image,
                f"image/{extension}",
                description=alt_text,
                file_name=f"image.{extension}",
            )

    except Exception as e:
        util.warn(f"Could not generate media post for {image_path}: {e}")

    return media_post


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def command_reply(status_text: str, reply_to: dict, command = "", image_path: str = None, image_alt = "") -> dict:
    """Reply to a user that issued a command.

    `status_text`: the content of the reply.
    `reply_to`: the status to reply to.
    `command`: the command that was replied to.
    `image_path`: path to an image to include in the post.
    """
    if dry:
        global poll_id
        poll_id += 1
        return {
            "id": poll_id
        }

    util.debug("Replying to command...")
    status_text = f"\n{status_text}"
    size = len(status_text) + len(reply_to["account"]["username"]) + len(command) + 4
    if size > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - size + len(status_text) - 4] + "..."
        util.warn("Tried to post a command reply exceeding the character limit.")
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    status = {}

    media_post = None
    if image_path:
        media_post = get_media_post(image_path, image_alt)
    mastodon.status(reply_to["id"])
    status = mastodon.status_reply(
        reply_to,
        status_text,
        untag=True,
        spoiler_text=util.config["command_prefix"] + command if command else None,
        media_ids=media_post,
        sensitive=bool(media_post)
    )
    util.debug(f"Replied to command")
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def post_status(status_text: str, last_result = "", chain=True, image_path: str = None, image_alt = "") -> dict:
    """Post a plain-text status, returning its data.

    `status_text`: text to populate the post with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    `image_path`: path to an image to include in the post.
    """
    util.info("Posting status")
    global status_chain

    if dry:
        global poll_id
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id,
            "created_at": datetime.now(timezone.utc),
            "url": ""
        }

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3] + "..."
        util.warn("Tried to post a status exceeding the character limit.")

    media_post = None
    if image_path:
        media_post = get_media_post(image_path, image_alt)

    status = mastodon.status_post(
        status_text,
        in_reply_to_id=status_chain if chain else None,
        media_ids=media_post,
        sensitive=bool(media_post)
    )
    status_chain = status["id"]
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def post_poll(status_text: str, last_result: str, options: list[str], chain=True, extend=False, is_multiple=False) -> dict:
    """Post a poll, returning its data.

    `status_text`: text to populate the poll body with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    `extend`: whether to extend the poll time beyond normal.
    """
    util.info("Posting poll")
    global status_chain

    if dry:
        global last_poll
        global poll_id
        last_poll = options
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id,
            "poll": {},
            "url": "",
            "created_at": str(datetime.now(timezone.utc))
        }

    duration = util.config["default_poll_duration"]
    if extend:
        duration *= util.config["no_vote_scale"]
    duration = max(min(duration, MAX_POLL_DURATION), MIN_POLL_DURATION)
    poll = mastodon.make_poll(options, duration, multiple=is_multiple)

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3] + "..."
        util.warn("Tried to post a poll exceeding the character limit.")

    for i in range(len(options)):
        if len(options[i]) > POLL_OPTION_LIMIT:
            options[i] = options[i][:POLL_OPTION_LIMIT - 3] + "..."

    status = mastodon.status_post(
        status_text,
        poll=poll,
        in_reply_to_id=status_chain if chain else None
    )
    status_chain = status["id"]
    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def delete_post(status_id: int) -> dict:
    """Delete the given post, returning its data.

    `status_id`: ID of the status to delete.
    """
    global status_chain

    if dry:
        if status_chain == status_id:
            status_chain = poll_id - 1

        return {
            "id": poll_id
        }

    status = mastodon.status_delete(status_id)
    if status["id"] == status_chain:
        status_chain = status["in_reply_to_id"]

    return status


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def poll_result(status_id: int, chain=True, option_groups: list = None, dry_skip: list[int] = None, multiple: int = 1) -> tuple[list[int], int, dict] | None:
    """Get poll results from the post with given ID.
    This is a blocking call and will wait until the poll is expired.

    If the queried poll allows for multiple votes, `option_groups` is ignored.

    `status_id`: ID of the status to query.
    `chain`: whether to chain (reply to) the previous status (applicable only if the poll is remade).
    `option_groups`: list of poll indexes to group and count together before individually.
    `dry_skip`: indices to not choose when running `--dry`
    `multiple`: how many poll options to pick

    Returns a tuple of (winning vote indices, number of votes, status)
    """
    util.debug("Waiting for poll to end...")
    global status_chain

    if dry:
        ranges = []
        for i in range(len(last_poll)):
            if dry_skip == None or i not in dry_skip:
                ranges.append(i)

        return ([random.choice(ranges)], 1, {"id": 0, "url": ""})

    sleep(util.config["default_poll_duration"] + 1)
    status = mastodon.status(status_id)

    if "poll" not in status:
        raise ValueError("Status does not contain a poll.")

    # update status info
    util.debug("Probing poll...")
    poll = mastodon.poll(status["poll"]["id"])

    # probe poll
    if not poll["expired"]:
        diff = poll["expires_at"] - datetime.now(timezone.utc)
        sleep(max(1, diff.total_seconds() + 1))

    poll = mastodon.poll(status["poll"]["id"])

    # No votes yet, remake poll for extended time
    if poll["votes_count"] <= 0:
        delete_post(status_id)
        return None

    # gather results
    util.debug("Polling results...")

    if option_groups is None:
        option_groups = []
    votes: list[int] = [result["votes_count"] for result in poll["options"]]

    # Add any index not in a group as its own group
    for i in range(len(votes)):
        if not util.int_in_array(option_groups, i):
            option_groups.append(i)

    option_groups = util.sort_multi(option_groups) # allows us to prefer lower index items on tie
    vote_order = util.flatten_array(sort_groups(votes, option_groups))
    # we only want results that are actually voted for
    # otherwise, multi-option polls return 0-vote options, too
    vote_order = [v for v in vote_order if votes[v] > 0]
    return (vote_order[:multiple], sum(votes), status)


def find_winning_index(votes: list[int], groups: list = None):
    """Find the winning index of an array of votes, given an arbitrarily nested
    int array of groups. For example, `[0, [1, [2, 3]]]` will first find a
    winner between `votes[0]` and `votes[1] + votes[2] + votes[3]`, then between
    `votes[1]` and `votes[2] + votes[3]` and so on. If a vote index is not
    present in groups, it will *not* be counted at all. Indices may be repeated.

    In the event of a tie, preference is given to the group at the lowest index.
    To prefer the lowest vote index, use `util.sort_multi` on `groups`.

    If `groups` is not provided, this function simply returns the vote index
    with the largest value.

    `votes`: list of vote counts.
    `groups`: arbitrarily nested vote indices to group together.
    """
    return util.flatten_array(sort_groups(votes, groups))[0]


def sort_groups(votes: list[int], groups: list = None):
    """Sorts the groups by their vote counts, with the "winner" being at index 0.
    `votes`: list of vote counts.
    `groups`: arbitrarily nested vote indices to group together.
    """
    if groups is None:
        groups = [i for i in range(len(votes))]
        groups.sort(key = lambda x: votes[x], reverse = True)
        return groups

    group_values = []
    groups_copy = deepcopy(groups)

    for i, group in enumerate(groups):
        if isinstance(group, list):
            sum_array = util.sum_array(group, lambda g: votes[g])
            group_values.append({ "index": i, "value": sum_array })
        else:
            group_values.append({ "index": i, "value": votes[group] })

    group_values.sort(key = lambda x: x["value"], reverse = True)

    for i, group in enumerate(groups):
        if isinstance(group, list):
            groups_copy[i] = sort_groups(votes, group)

    return [groups_copy[g["index"]] for g in group_values]


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def get_followers() -> set[str]:
    """Get the list of follower names.
    """
    if dry:
        followers = set()
        for _ in range(100):
            followers.add("".join(random.choices(string.printable, k=random.randint(1, 16))))
        return followers

    followers = mastodon.fetch_remaining(mastodon.account_followers(mastodon.me()))
    return {f["display_name"] for f in followers if str(f["id"]) not in util.blacklist["b"]}


@retry(
    stop=stop_after_attempt(util.config["max_retries"]),
    wait=wait_fixed(util.config["retry_delay"]),
    retry=retry_if_exception_type((MastodonError)),
    before_sleep=before_sleep_log(util.logger, logging.WARN))
def update_profile(bio: str) -> dict:
    """Update the user profile based upon the Character description.

    `bio`: the string to update the bot bio with.
    """
    if dry:
        return {
            "note": "This is a bio"
        }

    return mastodon.account_update_credentials(note=bio)


class CommandListener(StreamListener):

    def __init__(self) -> None:

        self._command_handlers: list[function] = []
        super().__init__()
        util.debug("Created command listener")


    # {
    #     'id': # id of the notification
    #     'type': # "mention", "reblog", "favourite", "follow", "poll" or "follow_request"
    #     'created_at': # The time the notification was created
    #     'account': # User dict of the user from whom the notification originates
    #     'status': # In case of "mention", the mentioning status
    #             # In case of reblog / favourite, the reblogged / favourited status
    # }
    def on_notification(self, notification: dict) -> None:

        try:
            if notification["type"] == "mention":
                util.debug(f"Received mention")
                for handler in self._command_handlers:
                    handler(notification["account"], notification["status"])

            super().on_notification(notification)
        except Exception as e:
            util.warn(f"Error encountered in command listener (mention): {e}")

    # {
    #     'id': # The ID of this conversation object
    #     'unread': # Boolean indicating whether this conversation has yet to be
    #             # read by the user
    #     'accounts': # List of accounts (other than the logged-in account) that
    #                 # are part of this conversation
    #     'last_status': # The newest status in this conversation
    # }
    def on_conversation(self, conversation: dict) -> None:

        try:
            util.debug("Received DM")
            for handler in self._command_handlers:
                handler(conversation["accounts"][0], conversation["last_status"])

            super().on_conversation(conversation)
        except Exception as e:
            util.warn(f"Error encountered in command listener (dm): {e}")


    def add_handler(self, callback) -> int:
        """Add a function to be called on mention or DM.
        """
        util.debug("Added command handler")
        self._command_handlers.append(callback)
        return len(self._command_handlers)
