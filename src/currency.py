from item import Item
from enum import Enum
import util
import math

class Unit(Enum):
    """Currency units.
    """
    cp = "cp"
    sp = "sp"
    ep = "ep"
    gp = "gp"
    pp = "pp"


# Currencies mapped to their full names.
_NAMES = {
    Unit.cp: "copper",
    Unit.sp: "silver",
    Unit.ep: "electrum",
    Unit.gp: "gold",
    Unit.pp: "platinum"
}

# Currencies mapped to their amount in Copper (CP).
_RATES = {
    Unit.cp: 1,
    Unit.sp: 10,
    Unit.ep: 50,
    Unit.gp: 100,
    Unit.pp: 1000
}


class Currency(Item):
    """A D&D-style currency. Arithmetic on this Currency only takes place
    to the degree of the minimum whole unit, Copper (CP).
    """

    def __init__(self, amount: float, unit = Unit.gp) -> None:
        """Create a Currency of the given amount and unit.

        `amount`: quantity of the Currency, will be truncated according to unit.
        `unit`: Unit of the Currency.
        """
        super().__init__(unit.value, _NAMES[unit].title())
        self.unit = unit
        self._amount = 0
        self.amount = amount
        self.cost = None
        self.emoji = util.emojis["equipment"]["gold"]


    @property
    def amount(self) -> float:

        return self._amount


    @amount.setter
    def amount(self, value) -> None:

        if not isinstance(value, int) and not isinstance(value, float):
            raise TypeError(f"Cannot set amount to type {type(value)}.")

        rate = _RATES[self.unit]
        self._amount = float(round(int(value * rate) / rate, len(str(rate))))


    @staticmethod
    def combine(*currencies: "Currency") -> "Currency":
        """Combine the given Currencies into a single Currency object. Returned
        Currency will be in terms of the largest whole unit (see `reduce()`).

        `currencies`: variable number of Currencies to combine.
        """
        new_currency = currencies[0]
        for currency in currencies[1:]:
            new_currency = new_currency.add(currency)
        return new_currency.reduce()


    @staticmethod
    def from_dict(dict: dict) -> "Currency":
        """Create a new Currency from a `cost` D&D API dict.

        `dict`: D&D API `cost` dict.
        """
        return Currency(dict["quantity"], Unit(dict["unit"]))


    def to(self, unit = Unit.gp, rounded = False) -> "Currency":
        """Convert the Currency to another unit, returning the new Currency.

        `unit`: currency unit to convert to.
        `rounded`: whether to round the result up to a whole unit.
        """
        exchanged = round((_RATES[self.unit] * self.amount) / _RATES[unit], len(str(_RATES[unit])))
        if rounded:
            exchanged = math.ceil(exchanged) if exchanged >= 0 else math.floor(exchanged)

        return Currency(exchanged, unit)


    def rounded(self) -> "Currency":
        """Round the Currency up to a whole unit, returning the new Currency.
        """
        return Currency(self.amount, self.unit).to(self.unit, True)


    def reduce(self) -> "Currency":
        """Collapse the currency into its nearest whole currency. I.E. 1000 GP
        = 100 PP, or 0.05 PP = 5 SP. Ignores Electrum (EP).
        """
        cp = self.to(Unit.cp)
        for unit in reversed(_RATES):
            if unit != unit.ep and cp.amount >= _RATES[unit]:
                return self.to(unit)

        return Currency(self.amount, self.unit)


    def equivalent(self, other: "Currency") -> bool:
        """Determine if this Currency is equivalent to the other. I.e. they
        represent the same amount of buying power, but may be in different units.

        `other`: another Currency to compare to.
        """
        return self.to(Unit.cp, False) == other.to(Unit.cp, False)


    def split(self) -> tuple["Currency"]:
        """Split this Currency into its largest whole parts, ignoring Electrum
        (EP). E.g.,
        ```
        5.432 PP = (5 PP, 4 GP, 3 SP, 2 CP)
        5.432 GP = (5 GP, 4 SP, 3 CP)
        5.432 SP = (5 SP, 4 CP)
        5.432 CP = (5 CP)
        ```
        """
        currencies = []
        cp = self.to(Unit.cp, False).amount
        pp, cp = divmod(cp, _RATES[Unit.pp])
        gp, cp = divmod(cp, _RATES[Unit.gp])
        sp, cp = divmod(cp, _RATES[Unit.sp])
        if pp > 0: currencies.append(Currency(pp, Unit.pp))
        if gp > 0: currencies.append(Currency(gp, Unit.gp))
        if sp > 0: currencies.append(Currency(sp, Unit.sp))
        if cp > 0: currencies.append(Currency(cp, Unit.cp))
        if not currencies: currencies.append(Currency(self.amount, self.unit))
        return tuple(currencies)


    def add(self, other: "Currency", rounded = False) -> "Currency":
        """Add `other` to this Currency and return a new Currency.

        `other`: Currency to add to this one.
        `rounded`: whether to round `other` up to this unit.
        """
        return Currency(self.amount + other.to(self.unit, rounded).amount, self.unit)


    def sub(self, other: "Currency", rounded = False) -> "Currency":
        """Subtract `other` from this Currency and return a new Currency.

        `other`: Currency to subtract from this one.
        `rounded`: whether to round `other` up to this unit.
        """
        return Currency(self.amount - other.to(self.unit, rounded).amount, self.unit)


    def __str__(self) -> str:

        return f"{self.amount} {self.unit.name.upper()}"

#region arithmetic operators

    def __add__(self, other: object) -> "Currency":

        if not isinstance(other, int)\
                and not isinstance(other, float)\
                and not isinstance(other, Currency):
            raise TypeError(f"Invalid type for Currency arithmetic: {type(other)}")

        new_currency = Currency(self.amount, self.unit)

        if isinstance(other, Currency):
            new_currency = new_currency.add(other)
        else:
            new_currency.amount += other

        return new_currency


    def __sub__(self, other: object) -> "Currency":

        if not isinstance(other, int)\
                and not isinstance(other, float)\
                and not isinstance(other, Currency):
            raise TypeError(f"Invalid type for Currency arithmetic: {type(other)}")

        new_currency = Currency(self.amount, self.unit)

        if isinstance(other, Currency):
            new_currency = new_currency.sub(other)
        else:
            new_currency.amount -= other

        return new_currency


    def __mul__(self, other: object) -> "Currency":

        if not isinstance(other, int) and not isinstance(other, float):
            raise TypeError(f"Invalid type for Currency arithmetic: {type(other)}")

        new_currency = Currency(self.amount, self.unit)
        new_currency.amount *= other
        return new_currency


    def __truediv__(self, other: object) -> "Currency":

        if not isinstance(other, int) and not isinstance(other, float):
            raise TypeError(f"Invalid type for Currency arithmetic: {type(other)}")

        new_currency = Currency(self.amount, self.unit)
        new_currency.amount /= other
        return new_currency

#endregion
#region comparison operators

    def __eq__(self, __value: object) -> bool:

        if not isinstance(__value, Currency):
            return False

        return __value.unit == self.unit and __value.amount == self.amount


    def __ne__(self, __value: object) -> bool:

        return not self.__eq__(__value)


    def __lt__(self, __value: object) -> bool:

        if not isinstance(__value, Currency):
            raise TypeError(f"Invalid type for Currency comparison: {type(__value)}")

        return self.to(Unit.cp, False).amount < __value.to(Unit.cp, False).amount


    def __le__(self, __value: object) -> bool:

        if not isinstance(__value, Currency):
            raise TypeError(f"Invalid type for Currency comparison: {type(__value)}")

        return self.to(Unit.cp, False).amount <= __value.to(Unit.cp, False).amount


    def __gt__(self, __value: object) -> bool:

        if not isinstance(__value, Currency):
            raise TypeError(f"Invalid type for Currency comparison: {type(__value)}")

        return self.to(Unit.cp, False).amount > __value.to(Unit.cp, False).amount


    def __ge__(self, __value: object) -> bool:

        if not isinstance(__value, Currency):
            raise TypeError(f"Invalid type for Currency comparison: {type(__value)}")

        return self.to(Unit.cp, False).amount >= __value.to(Unit.cp, False).amount

#endregion
