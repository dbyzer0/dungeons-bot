# Contributing

The project is mostly maintained by one person and undergoes frequent system changes and rewrites. I would suggest only making low-scope contributions for the time being.

Consider joining the [Dev room in the Matrix space](https://matrix.to/#/#dungeonsdev:matrix.org) if you make frequent contributions or wish to discuss development.

## Issues

If you encounter a bug or want a feature added, [open an issue here](https://gitlab.com/ASTRELION/dungeons-bot/-/issues/new), and use the relevant issue template.

## Code

1. [Fork the repository](https://gitlab.com/ASTRELION/dungeons-bot/-/forks/new)
2. Clone your fork locally
3. Create a branch for your changes based on the `develop` branch
4. Make your changes
5. Run tests (these tests do not offer great coverage, make sure to test yourself locally, ideally with a test Mastodon account)
    ```shell
    python3 -m pytest
    ```
6. Commit and push your changes
7. [Submit a Merge Request (MR)](https://gitlab.com/ASTRELION/dungeons-bot/-/merge_requests/new) (same as a Pull Request (PR)) to the `develop` branch of the original repository

### Dev Environment

Create a virtual environment

```bash
python -m venv .venv
source .venv/bin/activate
```

Install packages

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

### Database

#### Generate migrations

```
alembic -c config/alembic.ini revision --autogenerate
```

#### Upgrade Remote DB

*This will automatically happen when running the bot.*

```
alembic -c config/alembic.ini upgrade head
```

### Description of files

**Root folders**
| Folder | Description |
|--------|-------------|
| `config/` | Contains configuration files. These are copied and used from `data/` when the program runs. The files in `config/` are used as defaults, so when modifying configuration values for an instance you should modify the data in `data/`. |
| `data/` | The program uses this directory for all data files, but is not actually part of the repository. If you need to add a `.token`, it should go here before running. |
| `images/` | Branding images, logos, banners, etc. |
| `public/` | Static website used for https://dungeons.astrelion.com. |
| `src/` | Source code files. |
| `tests/` | Any file in this directory will be ran as tests for `pytest`. Each file corresponds to a file in `src/` and follows the format `test_[src_file_name]`. |

**Source files (`src/*`)**

| File | Description |
|------|-------------|
| `__main__.py` | Main entrypoint for the program. You can either run it with `python3 src` or `python3 src/__main__.py`. Responsible for the "epic" loop, initializing the clients, etc. |
| `campaign.py` | This is where the bulk of the state logic happens. Campaigns also control what data is saved/loaded to file and command outputs. |
| `character.py` | Represents a character during a campaign. Inherits from `creature` and performs all the character-related logic like equipping items, combat, etc. |
| `client.py` | Handles all Mastodon-related requests, resolving polls, tips, command posts, updated images, etc. |
| `creature.py` | Base class of `Monster` and `Character`. Contains all the base logic for any animate creature in the game and handles basics like managing health, creature data, etc. |
| `currency.py` | Contains the `Currency` class and handles all the logic pertaining to evaluating currency rolls, conversions, etc. |
| `dice.py` | Contains the `Dice` class and handles all the logic pertaining to dice rolls. |
| `dnd.py` | Handles all D&D API related requests. |
| `effect.py` | Contains all the logic for handling temporary or permanent status effects. |
| `equipment.py` | Represents an equipment item, like armor or weapons.
| `image.py` | Handles all AI Horde API related requests and image processing. |
| `item.py` | Base class for basically anything that can be equipped. |
| `lang.py` | Contains all the logic for parsing the `lang.toml` file. If a new root node is added to `lang.toml`, a relevant variable needs to be added here as well. |
| `magic_item.py` | Represents a magic item, and currently contains all Potions as well. |
| `monster.py` | Represents a Monster. Inherits from `creature` and contains all monster-related logic like combat, stats, etc. |
| `util.py` | Contains various utility functions and variables. |
