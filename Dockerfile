FROM python:3.10-alpine

# Source
ADD src/*.py /src/
ADD src/dnd/*.py /src/dnd/

# Configs
ADD config/*.toml /config/
ADD config/.token.example /config/.token.example

# DB
ADD config/alembic.ini /config/alembic.ini
ADD alembic/ /alembic/

# Packages
ADD requirements.txt /

RUN pip install -r /requirements.txt

CMD ["python", "/src/__main__.py", "--verbose"]
